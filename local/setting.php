<?php 
define('CNF_APPNAME','Spring Air');
define('CNF_APPDESC','Fragrances & Creations');
define('CNF_COMNAME','Spring Air');
define('CNF_EMAIL','geral@springair.com.pt');
define('CNF_PHONE','+351 919 472 175');
define('CNF_METAKEY','spring air');
define('CNF_METADESC','Spring Air');
define('CNF_GROUP','3');
define('CNF_ACTIVATION','auto');
define('CNF_MULTILANG','0');
define('CNF_LANG','en');
define('CNF_REGIST','false');
define('CNF_FRONT','true');
define('CNF_RECAPTCHA','false');
define('CNF_THEME','default');
define('CNF_RECAPTCHAPUBLICKEY','');
define('CNF_RECAPTCHAPRIVATEKEY','');
define('CNF_MODE','production');
define('CNF_LOGO','backend-logo.svg');
define('CNF_ALLOWIP','');
define('CNF_RESTRICIP','192.116.134 , 194.111.606.21 ');
define('CNF_MAIL','phpmail');
define('CNF_DATE','m/d/y');
?>