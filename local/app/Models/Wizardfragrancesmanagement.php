<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class wizardfragrancesmanagement extends Sximo  {
	
	protected $table = 'wizard_fragrances';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT wizard_fragrances.* FROM wizard_fragrances  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE wizard_fragrances.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
