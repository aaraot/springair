<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class valuesmanagement extends Sximo  {
	
	protected $table = 'bvalues';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT bvalues.* FROM bvalues  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE bvalues.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
