<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class quotesmanagement extends Sximo  {
	
	protected $table = 'quotes';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT quotes.* FROM quotes  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE quotes.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
