<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class budgetsmanagement extends Sximo  {
	
	protected $table = 'budgets';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT budgets.* FROM budgets  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE budgets.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
