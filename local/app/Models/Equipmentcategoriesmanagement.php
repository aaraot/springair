<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class equipmentcategoriesmanagement extends Sximo  {
	
	protected $table = 'equipment_categories';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT equipment_categories.* FROM equipment_categories  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE equipment_categories.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
