<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class searchsuggestionsmanagement extends Sximo  {
	
	protected $table = 'search_suggestion';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT search_suggestion.* FROM search_suggestion  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE search_suggestion.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
