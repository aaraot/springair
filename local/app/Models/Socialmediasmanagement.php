<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class socialmediasmanagement extends Sximo  {
	
	protected $table = 'social_medias';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT social_medias.* FROM social_medias  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE social_medias.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
