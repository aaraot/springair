<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class servicehighlightsmanagement extends Sximo  {
	
	protected $table = 'service_highlights';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT service_highlights.* FROM service_highlights  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE service_highlights.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
