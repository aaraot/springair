<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class equipmentsmanagement extends Sximo  {
	
	protected $table = 'equipments';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT equipments.* FROM equipments  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE equipments.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
