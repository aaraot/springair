<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class client extends Sximo  {
	
	protected $table = 'Client';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT Client.* FROM Client  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE Client.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
