<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class businessesmanagement extends Sximo  {
	
	protected $table = 'businesses';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT businesses.* FROM businesses  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE businesses.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
