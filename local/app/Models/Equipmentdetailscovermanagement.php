<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class equipmentdetailscovermanagement extends Sximo  {
	
	protected $table = 'equipment_details_cover';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT equipment_details_cover.* FROM equipment_details_cover  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE equipment_details_cover.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
