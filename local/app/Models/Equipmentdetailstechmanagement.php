<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class equipmentdetailstechmanagement extends Sximo  {
	
	protected $table = 'equipment_details_tech';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT equipment_details_tech.* FROM equipment_details_tech  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE equipment_details_tech.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
