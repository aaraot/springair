<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class copiesmanagement extends Sximo  {
	
	protected $table = 'copies';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT copies.* FROM copies  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE copies.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
