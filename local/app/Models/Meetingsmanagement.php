<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class meetingsmanagement extends Sximo  {
	
	protected $table = 'meetings';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT meetings.* FROM meetings  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE meetings.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
