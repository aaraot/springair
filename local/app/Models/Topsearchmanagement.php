<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class topsearchmanagement extends Sximo  {
	
	protected $table = 'top_search';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT top_search.* FROM top_search  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE top_search.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
