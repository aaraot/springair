<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class spacesmanagement extends Sximo  {
	
	protected $table = 'spaces';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT spaces.* FROM spaces  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE spaces.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
