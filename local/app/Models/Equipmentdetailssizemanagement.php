<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class equipmentdetailssizemanagement extends Sximo  {
	
	protected $table = 'equipment_details_size';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT equipment_details_size.* FROM equipment_details_size  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE equipment_details_size.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
