<?php

use App\Models\Core\Pages;

class PageHelpers
{
    
    public static function saveSection()
    {
	/* History*/
	$temp_page_id = Input::get('pageID');
	$temp_page = \DB::table('tb_pages')->where('pageID','=',$temp_page_id)->first();
	\DB::table('tb_pages_history')->insert(['pageID' => $temp_page->pageID, 'content' => $temp_page->content]);
	
        $data['content'] = Input::get('content');
        $model = new Pages();
        $model->insertRow($data, Input::get('pageID'));
       
        return 'success';
    }

    public static function pages($data, $page)
    {
        $row = DB::select('SELECT * FROM tb_pages WHERE pageID = "' . $page . '"')[0];
        $data['pageID'] = $row->pageID;
        $data['pages'] = 'pages.partials.html'; //.$row->filename;
	$data['filename'] = $row->filename;
        $data['transparent'] = $row->transparent;
        return $data;
    }

    public static function html($pageID){
        $content = DB::select('SELECT content FROM tb_pages WHERE pageID = "' . $pageID . '"')[0];
        return $content->content;
    }
    
     public static function history($pageID){
        $history = DB::select('SELECT * FROM tb_pages_history WHERE pageID = "' . $pageID . '" ORDER BY created_at DESC LIMIT 10');
        return $history;
    }
}