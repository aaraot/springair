<?php namespace App\Library;

use App;
use App\Models\Websites;
use DB;
use Auth;
use Input;
use Request;
use Session;
//use App\Library\MultiSiteHelpers;

class WebsiteHelpers
{

    /*
     * Get current website
     */
    public static function getWebsite()
    {
        //return Session::get('APP_ENV') == '' ? $_SERVER['HTTP_HOST'] : Session::get('APP_ENV');
        Session::put('APP_ENV', 'mediaweb.pt');
        return Session::get('APP_ENV');
    }
    
    public static function getSite()
    {
        return Websites::where('website', 'mediagest.pt')->first();
    }

    /*
     * Get website_id
     */
    public static function getWebsiteId()
    {
        //return Websites::where('website', WebsiteHelpers::getWebsite())->get()[0]->website_id;
        return Websites::where('website', 'mediagest.pt')->get()[0]->website_id;
    }

    /*
     * Get website_id by name
     */
    public static function getWebsiteIdByName($website)
    {
        //return Websites::where('website', $website)->get()[0]->website_id;
        return Websites::where('website', 'mediagest.pt')->get()[0]->website_id;
    }

    public static function getWebsiteNameById($id){
        return Websites::where('website_id', $id)->get()[0]->website;
    }

    /*
     * Get website_name
     */
    public static function getWebsiteName()
    {
        return Websites::where('website', WebsiteHelpers::getWebsite())->get()[0]->website;
    }

    /*
     * Get websites
     */
    public static function getWebsites()
    {
        $permissions = explode(',', Auth::user()->websites);

        $websites = Websites::select('website')->whereIn('website_id', $permissions)->get();

        return $websites;
    }

    /*
     * Get website header
     */
    public static function getHeader()
    {
        //return Websites::where('website_id', MultiSiteHelpers::getWebsite()['website_id'])->get()[0]->header;
        return Websites::where('website', 'mediagest.pt')->get()[0]->header;
    }
    
    /*
     * Get website footer
     */
    public static function getFooter()
    {
        //return Websites::where('website_id', MultiSiteHelpers::getWebsite()['website_id'])->get()[0]->header;
          return Websites::where('website', 'mediagest.pt')->get()[0]->footer;
    }

    /*
     * Get website style
     */
    public static function getStyle()
    {
        /*return Websites::leftJoin('website_styles', 'website_styles.name', '=', 'websites.style')
            ->where('website_id', MultiSiteHelpers::getWebsite()['website_id'])->first()->theme;*/
            
        return Websites::leftJoin('website_styles', 'website_styles.name', '=', 'websites.style')
            ->where('website_id','1')->first()->theme;
    }

}