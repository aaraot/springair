<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'pageTitle' => CNF_APPNAME,
            'pageNote' => 'Welcome to Dashboard',
        );
    }

    public function getIndex(Request $request)
    {
        return view('dashboard.index', $this->data);
    }
}