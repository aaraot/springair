<?php namespace App\Http\Controllers;

use App\Models\Budgetsmanagement;
use App\Models\Businessesmanagement;
use App\Models\Clientsmanagement;
use App\Models\Contactsmanagement;
use App\Models\Copiesmanagement;
use App\Models\Equipmentcategoriesmanagement;
use App\Models\Equipmentdetailscovermanagement;
use App\Models\Equipmentdetailssizemanagement;
use App\Models\Equipmentdetailstechmanagement;
use App\Models\Equipmentsmanagement;
use App\Models\Imagesmanagement;
use App\Models\Meetingsmanagement;
use App\Models\Pagesmanagement;
use App\Models\Quotesmanagement;
use App\Models\Searchsuggestionsmanagement;
use App\Models\Seomanagement;
use App\Models\Servicehighlightsmanagement;
use App\Models\Servicesmanagement;
use App\Models\Socialmediasmanagement;
use App\Models\Spacesmanagement;
use App\Models\Topsearchmanagement;
use App\Models\Valuesmanagement;
use App\Models\Wizardfragrancesmanagement;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    function index(Request $request)
    {
        $obj = self::seo(1);

        if ($request->get('type') === 'seo') {
            return $obj;
        } else {
            $page = $obj;

            $obj = self::objects($page, self::isMobile($request));

            if ($obj['isMobile']) {
                if ($request->get('fetchContent')) {
                    return view('pages.mobile.home-content', ['obj' => $obj]);
                } else {
                    return view('pages.home', ['obj' => $obj]);
                }
            } else {
                return view('pages.home', ['obj' => $obj]);
            }
        }
    }

    function seo($id)
    {
        $seo = seomanagement::find($id);

        return [
            'title' => $seo->title,
            'description' => $seo->description,
            'keywords' => $seo->keywords
        ];
    }

    function objects($page, $isMobile)
    {
        $socials = socialmediasmanagement::all();
        $copies = copiesmanagement::all();
        $pages = pagesmanagement::all();
        $searchSuggestions = self::pagesURL(searchsuggestionsmanagement::all());
        $topSearches = self::pagesURL(topsearchmanagement::all());
        $images = imagesmanagement::all();
        $clients = clientsmanagement::all();
        $quotes = quotesmanagement::all();
        $services = servicesmanagement::all();
        $servicesHighlights = servicehighlightsmanagement::all();
        $spaces = spacesmanagement::all();
        $businesses = businessesmanagement::all();
        $values = valuesmanagement::all();
        $categories = equipmentcategoriesmanagement::all();
        $equipments = equipmentsmanagement::all();

        return [
            'page' => $page,
            'socials' => $socials,
            'copies' => $copies,
            'pages' => $pages,
            'images' => $images,
            'searchSuggestions' => $searchSuggestions,
            'topSearches' => $topSearches,
            'services' => $services,
            'servicesHighlights' => $servicesHighlights,
            'spaces' => $spaces,
            'businesses' => $businesses,
            'values' => $values,
            'clients' => $clients,
            'quotes' => $quotes,
            'categories' => $categories,
            'equipments' => $equipments,
            'isMobile' => $isMobile
        ];
    }

    function pagesURL($obj)
    {
        foreach ($obj as $item) {
            if ($item->page_id) {
                $item->page_url = str_slug(pagesmanagement::find($item->page_id)->name);
            }
            if ($item->equipment_id) {
                $item->equipment_url = 'equipamentos#' . str_slug(equipmentsmanagement::find($item->equipment_id)->name);
            }
        }

        return $obj;
    }

    function isMobile($request)
    {
        $useragent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent)
            || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            return true;
        } else {
            return false;
        }
    }

    function search(Request $request)
    {
        $view = $request->get('mobile') === 'false' ? 'layouts.springair.components.header.search.search-results' : 'layouts.springair.components.header.search.search-results-mb';

        $results = [
            'equipments' => [],
            'services' => []
        ];

        $equipments = Equipmentsmanagement::join('equipment_categories', 'equipments.equipment_category_id', '=', 'equipment_categories.id')
            ->join('equipment_details_tech', 'equipments.id', '=', 'equipment_details_tech.equipment_id')
            ->join('equipment_details_cover', 'equipments.id', '=', 'equipment_details_cover.equipment_id')
            // Equipment
            ->orWhere('equipment_categories.name', 'like', '%' . $request->get('search') . '%')
            ->orWhere('equipments.name', 'like', '%' . $request->get('search') . '%')
            ->orWhere('equipments.description', 'like', '%' . $request->get('search') . '%')
            // Techs
            ->orWhere('equipment_details_tech.name', 'like', '%' . $request->get('search') . '%')
            ->orWhere('equipment_details_tech.description', 'like', '%' . $request->get('search') . '%')
            // Covers
            ->orWhere('equipment_details_cover.name', 'like', '%' . $request->get('search') . '%')
            // Select
            ->select('equipments.image_search as image', 'equipments.name', 'equipments.description')
            ->get();

        foreach ($equipments as $equipment) {
            if (!in_array($equipment, $results['equipments'])) {
                array_push($results['equipments'], $equipment);
            }
        }

        $services = Servicesmanagement::join('service_highlights', 'services.id', '=', 'service_highlights.service_id')
            // Service
            ->orWhere('services.title', 'like', '%' . $request->get('search') . '%')
            ->orWhere('services.description', 'like', '%' . $request->get('search') . '%')
            // Highlights
            ->orWhere('service_highlights.title', 'like', '%' . $request->get('search') . '%')
            ->orWhere('service_highlights.description', 'like', '%' . $request->get('search') . '%')
            // Select
            ->select('services.search_image as image', 'services.title', 'services.description')
            ->get();

        foreach ($services as $service) {
            if (!in_array($service, $results['services'])) {
                array_push($results['services'], $service);
            }
        }

        return view($view)->with('results', $results);
    }

    function meeting(Request $request)
    {
        \DB::beginTransaction();

        try {
            $meeting = new meetingsmanagement();

            $meeting->name = $request->get('data')['name'];
            $meeting->email = $request->get('data')['email'];
            $meeting->phone = $request->get('data')['phone'];
            $meeting->message = $request->get('data')['message'];
            $meeting->date = $request->get('data')['date'];
            $meeting->hour = $request->get('data')['hour'];

            $meeting->save();

            \DB::commit();

            \Mail::send('emails.meeting', ['meeting' => $meeting], function ($m) use ($meeting) {
                $m->from('geral@infinityair.com.pt', 'Website');

                $m->to(['antoniofortunato@springair.com.pt', 'anacharters@springair.com.pt'], 'Springair Portugal')->subject('[Reunião] Contacto Website');
            });
        } catch (\Exception $e) {
            \DB::rollback();

            return $e;
        }

        return \Mail::failures() ? ['submitted' => false] : ['submitted' => true];
    }

    function wizardFragrances(Request $request)
    {
        \DB::beginTransaction();

        try {
            $wfragrance = new wizardfragrancesmanagement();

            $wfragrance->name = $request->get('data')['name'];
            $wfragrance->email = $request->get('data')['email'];
            $wfragrance->phone = $request->get('data')['phone'];
            $wfragrance->message = $request->get('data')['message'];
            $wfragrance->space = $request->get('data')['space'];
            $wfragrance->business = $request->get('data')['business'];
            $wfragrance->dimensions = $request->get('data')['dimensions'];

            $array = '';
            foreach ($request->get('data')['attributes'] as $key => $attributes) {
                $divider = ', ';
                if ($key === (count($request->get('data')['attributes']) - 1)) {
                    $divider = '';
                }
                $array .= $attributes . $divider;
            }
            $wfragrance->attributes = $array;

            $wfragrance->save();

            \DB::commit();

            \Mail::send('emails.fragrances', ['wfragrance' => $wfragrance], function ($m) use ($wfragrance) {
                $m->from('geral@infinityair.com.pt', 'Website');

                $m->to(['antoniofortunato@springair.com.pt', 'anacharters@springair.com.pt'], 'Springair Portugal')->subject('[Fragrâncias] Contacto Website');
            });
        } catch (\Exception $e) {
            \DB::rollback();

            return $e;
        }

        return \Mail::failures() ? ['submitted' => false] : ['submitted' => true];
    }

    function equipments(Request $request)
    {
        if ($request->get('type') === 'seo') {
            return self::seo(2);
        } else {
            $page = self::seo(2);

            $obj = self::objects($page, self::isMobile($request));

            if ($obj['isMobile']) {
                if ($request->get('fetchContent')) {
                    return view('pages.mobile.equipments-content', ['obj' => $obj]);
                } else {
                    return view('pages.equipments', ['obj' => $obj]);
                }
            } else {
                return view('pages.home', ['obj' => $obj]);
            }
        }
    }

    function filterEquipments(Request $request)
    {
        $equipments = count($request->get('id')) === 0 ? equipmentsmanagement::all() : self::getEquipments($request->get('id'));

        $obj = self::objects(self::seo(2), false);

        return view('pages.sections.equipments.filter-equipment', ['equipments' => $equipments, 'obj' => $obj]);
    }

    function getEquipments($ids)
    {
        $allEquipments = equipmentsmanagement::all();
        $equipments = [];

        foreach ($allEquipments as $equipment) {
            $add = false;
            foreach ($ids as $id) {
                $add = str_contains($equipment->equipment_category_id, $id) ? true : false;

                if (!$add) {
                    break;
                }
            }

            if ($add) {
                array_push($equipments, $equipment);
            }
        }

        return $equipments;
    }

    function equipmentsDetail(Request $request)
    {
        $view = $request->get('mobile') === 'false' ? 'pages.sections.equipments.equipment-detail' : 'pages.sections.equipments.equipment-detail-mb';

        return view($view)->with('copies', copiesmanagement::all())
            ->with('equipment', equipmentsmanagement::find($request->get('id')))
            ->with('size', equipmentdetailssizemanagement::where('equipment_id', $request->get('id'))->first())
            ->with('techs', equipmentdetailstechmanagement::where('equipment_id', $request->get('id'))->get())
            ->with('cover', equipmentdetailscovermanagement::where('equipment_id', $request->get('id'))->first());
    }

    function budget(Request $request)
    {
        \DB::beginTransaction();

        try {
            $budget = new budgetsmanagement();

            $budget->name = $request->get('data')['name'];
            $budget->email = $request->get('data')['email'];
            $budget->phone = $request->get('data')['phone'];
            $budget->message = $request->get('data')['message'];

            $budget->save();

            \DB::commit();

            \Mail::send('emails.budget', ['budget' => $budget], function ($m) use ($budget) {
                $m->from('geral@infinityair.com.pt', 'Website');

                $m->to(['antoniofortunato@springair.com.pt', 'anacharters@springair.com.pt'], 'Springair Portugal')->subject('[Orçamentos] Contacto Website');
            });
        } catch (\Exception $e) {
            \DB::rollback();

            return $e;
        }

        return \Mail::failures() ? ['submitted' => false] : ['submitted' => true];
    }

    function services(Request $request)
    {
        $pages = pagesmanagement::all();

        foreach ($pages as $page) {
            if (str_slug($page->name) === explode('/', $request->getPathInfo())[1]) {
                $id = $page->id;
            }
        }

        $obj = self::seo($id);

        if ($request->get('type') === 'seo') {
            return $obj;
        } else {
            $page = $obj;

            $obj = self::objects($page, self::isMobile($request));

            foreach ($obj['services'] as $service) {
                if (str_contains(explode('/', $request->getPathInfo())[1], str_slug($service->title))) {
                    $obj['service_id'] = $service->id;
                }
            }

            if ($obj['isMobile']) {
                if ($request->get('fetchContent')) {
                    return view('pages.mobile.service-content', ['obj' => $obj]);
                } else {
                    return view('pages.service', ['obj' => $obj]);
                }
            } else {
                return view('pages.home', ['obj' => $obj]);
            }
        }
    }

    function about(Request $request)
    {
        $obj = self::seo(6);

        if ($request->get('type') === 'seo') {
            return $obj;
        } else {
            $page = $obj;

            $obj = self::objects($page, false);

            return view('pages.home', ['obj' => $obj]);
        }
    }

    function contacts(Request $request)
    {
        $obj = self::seo(7);

        if ($request->get('type') === 'seo') {
            return $obj;
        } else {
            $page = $obj;

            $obj = self::objects($page, self::isMobile($request));

            if ($obj['isMobile']) {
                if ($request->get('fetchContent')) {
                    return view('pages.mobile.contacts-content', ['obj' => $obj]);
                } else {
                    return view('pages.contacts', ['obj' => $obj]);
                }
            } else {
                return view('pages.home', ['obj' => $obj]);
            }
        }
    }

    function contact(Request $request)
    {
        \DB::beginTransaction();

        try {
            $contact = new contactsmanagement();

            $contact->name = $request->get('data')['name'];
            $contact->email = $request->get('data')['email'];
            $contact->phone = $request->get('data')['phone'];
            $contact->message = $request->get('data')['message'];

            $contact->save();

            \DB::commit();

            \Mail::send('emails.contacts', ['contact' => $contact], function ($m) use ($contact) {
                $m->from('geral@infinityair.com.pt', 'Website');

                $m->to(['antoniofortunato@springair.com.pt', 'anacharters@springair.com.pt'], 'Springair Portugal')->subject('[Contactos] Contacto Website');
            });
        } catch (\Exception $e) {
            \DB::rollback();

            return $e;
        }

        return \Mail::failures() ? ['submitted' => false] : ['submitted' => true];
    }
}