<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$active_multilang = defined('CNF_MULTILANG') ? CNF_LANG : 'en';
\App::setLocale($active_multilang);
if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {
    $lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
    \App::setLocale($lang);
}

// Search
Route::post('/search', 'HomeController@search');

// Home
Route::get('/', 'HomeController@index');
Route::post('/meeting', 'HomeController@meeting');
Route::post('/wizard-fragrances', 'HomeController@wizardFragrances');
// Equipments
Route::get('/equipamentos', 'HomeController@equipments');
Route::post('/filter-equipments', 'HomeController@filterEquipments');
Route::post('/equipments-detail', 'HomeController@equipmentsDetail');
Route::post('/budget', 'HomeController@budget');
// Services
Route::get('/aromatizacao-de-espacos', 'HomeController@services');
Route::get('/logo-olfativo', 'HomeController@services');
Route::get('/remocao-de-odores', 'HomeController@services');
// About us
Route::get('/sobre-nos', 'HomeController@about');
// Contacts
Route::get('/contactos', 'HomeController@contacts');
Route::post('/contact', 'HomeController@contact');

Route::controller('/user', 'UserController');

include('pageroutes.php');
include('moduleroutes.php');

Route::resource('sximoapi', 'SximoapiController');
Route::group(['middleware' => 'auth'], function () {

    Route::get('core/elfinder', 'Core\ElfinderController@getIndex');
    Route::post('core/elfinder', 'Core\ElfinderController@getIndex');
    Route::controller('/dashboard', 'DashboardController');
    Route::controllers([
        'core/users' => 'Core\UsersController',
        'notification' => 'NotificationController',
        'core/logs' => 'Core\LogsController',
        'core/pages' => 'Core\PagesController',
        'core/groups' => 'Core\GroupsController',
        'core/template' => 'Core\TemplateController'
    ]);

});

Route::group(['middleware' => 'auth', 'middleware' => 'sximoauth'], function () {

    Route::controllers([
        'sximo/menu' => 'Sximo\MenuController',
        'sximo/config' => 'Sximo\ConfigController',
        'sximo/module' => 'Sximo\ModuleController',
        'sximo/tables' => 'Sximo\TablesController',
        'sximo/code' => 'Sximo\CodeController'
    ]);

});