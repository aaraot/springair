<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Blade;
use App\Models\Termsmanagement;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive("term", function ($expression) {
            return Termsmanagement::where('name', str_replace(array( '(', ')' ), '', $expression))->first()->text;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
