<style>
    #builder {
        width: 100%;
        height: 800px;
        position: relative;
    }

    iframe {
        width: 100%;
        height: 100%;
    }

    /*span.togglebutton {
        position: absolute;
        right: 17px;
        top: 0;
        background-color: black;
        padding: 3px;
        color: white;
        cursor: pointer;
    }*/
    
    span.togglebutton {
        position: absolute;
        top: 0;
        background-color: transparent;
        padding: 3px;
        color: white;
        cursor: pointer;
        width: 50px;
        height: 50px;
        text-align: center;
        vertical-align: text-bottom;
    }

    /*.togglebutton.i {
        font-size: 13px;
    }*/
    
    .togglebutton.i {
        font-size: 17px;
        padding: 15px;
    }
    
    #builder.maximized {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border: 0 none;
        z-index: 100000;
    }
</style>

<script>
    $(document).ready(function () {
        //$('#builder').append('<span class="togglebutton">Toggle  <i class="fa fa-arrows-alt"></i></span>');
        $('#builder').append('<span class="togglebutton"></span>');

        toggle();

        $("span.togglebutton").click(function () {
            toggle();
        });
    });

    function toggle() {
        $('#builder').toggleClass("maximized");

        if ($(".maximized")[0])
            $("body").css("overflow", "hidden");
        else
            $("body").css("overflow", "auto");
    }
</script>

<div id="builder" class="" style="background:#fff;">
    <iframe src="{{ URL::to($url) }}" frameBorder="0"></iframe>
</div>