@extends('layouts.app')

@section('content')
	
<style type="text/css">
    .mce-i-browse {
    padding-right: 20px !important;
    }
    div.mce-fullscreen {
        z-index: 1002 !important;
    }
</style>    
<script type="text/javascript" src="{{ asset('sximo/js/plugins/tinymce_new/js/tinymce/tinymce.min.js') }}"></script>  

<div class="page-content row">
<div class="page-content-wrapper">
	@if(Session::has('message'))	  
		   {{ Session::get('message') }}
	@endif	
		
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
		 {!! Form::open(array('url'=>'core/pages/save/'.$row['pageID'], 'class'=>'form-vertical row ','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

			<div class="col-sm-8 ">
				<div class="sbox">
					<div class="sbox-title">Page Content </div>	
					<div class="sbox-content">		

						<ul class="nav nav-tabs" >
						  <li class="active"><a href="#info" data-toggle="tab"> Page Content </a></li>
						  <li ><a href="#meta" data-toggle="tab"> Meta & Description </a></li>
						</ul>	

						<div class="tab-content">
						  <div class="tab-pane active m-t" id="info">
							  <div class="form-group  " >
								
								<div class="" style="background:#fff;">
								  <!-- Editor Files -->
								  <!--<textarea name='content' rows='35' id='content'    class='form-control markItUp'  
									 >{{-- htmlentities($content) --}}</textarea>-->
								  
								  <!-- Editor SQL -->
								  <textarea id="content" name="content" >{{ $row['content'] }}</textarea>
								 </div> 
							  </div> 						  

						  </div>

						  <div class="tab-pane m-t" id="meta">

					  		<div class="form-group  " >
								<label class=""> Metakey </label>
								<div class="" style="background:#fff;">
								  <textarea name='metakey' rows='5' id='metakey' class='form-control markItUp'>{{ $row['metakey'] }}</textarea> 
								 </div> 
							  </div> 

				  			<div class="form-group  " >
								<label class=""> Meta Description </label>
								<div class="" style="background:#fff;">
								  <textarea name='metadesc' rows='10' id='metadesc' class='form-control markItUp'>{{ $row['metadesc'] }}</textarea> 
								 </div> 
							  </div> 							  						  

						  </div>

						</div>  
						
	
					 </div>
				</div>	
		 	</div>		 
		 
		 <div class="col-sm-4 ">
			<div class="sbox">
				<div class="sbox-title">Page Info </div>	
				<div class="sbox-content">						
				  <div class="form-group hidethis " style="display:none;">
					<label for="ipt" class=""> PageID </label>
					
					  {!! Form::text('pageID', $row['pageID'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
					
				  </div> 					
				  <div class="form-group  " >
					<label for="ipt" > Title </label>
					
					  {!! Form::text('title', $row['title'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'id'=>'title' )) !!} 
					
				  </div> 					
				  <div class="form-group  " >
					<label for="ipt" > Alias </label>
					
					  {!! Form::text('alias', $row['alias'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true' , 'id'=>'alias' , 'pattern' => '^[a-z_-]+$' )) !!} 
					 
				  </div> 					
				  <div class="form-group  " >
					<label for="ipt" > Filename </label>
					
					  <input id="filename" name="filename" type="text" class="form-control" value="{{ $row['filename']}}" 
					  @if($row['pageID'] !='') readonly="1" @endif pattern="^[a-z]+$" required
					  />
					
				  </div> 
				  <div class="form-group  " >
				  <label for="ipt"> Who can view this page ? </label>
					@foreach($groups as $group) 
					<label class="checkbox">					
					  <input  type='checkbox' name='group_id[{{ $group['id'] }}]'    value="{{ $group['id'] }}"
					  @if($group['access'] ==1 or $group['id'] ==1)
					  	checked
					  @endif				 
					   /> 
					  {{ $group['name'] }}
					</label>  
					@endforeach	
						  
				  </div> 
				  <div class="form-group  " >
					<label> Show for Guest ? unlogged  </label>
					<label class="checkbox"><input  type='checkbox' name='allow_guest' 
 						@if($row['allow_guest'] ==1 ) checked  @endif	
					   value="1"	/> Allow Guest ?  </lable>
				  </div>


				  <div class="form-group hidethis " style="display:none;">
					<label for="ipt" class=" control-label col-md-4 text-right"> Created </label>
					<div class="col-md-8">
					  {!! Form::text('created', $row['created'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
					 </div> 
				  </div> 					
				  <div class="form-group hidethis " style="display:none;">
					<label for="ipt" > Updated </label>
			
					  {!! Form::text('updated', $row['updated'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
					
				  </div>	
	
				  <div class="form-group  " >
					<label> Status </label>
					<label class="radio">					
					  <input  type='radio' name='status'  value="enable" required
					  @if( $row['status'] =='enable')  	checked	  @endif				  
					   /> 
					  Enable
					</label> 
					<label class="radio">					
					  <input  type='radio' name='status'  value="disabled" required
					   @if( $row['status'] =='disabled')  	checked	  @endif				  
					   /> 
					  Disabled
					</label> 					 
				  </div> 

				  <div class="form-group  " >
					<label> Template </label>
					<label class="radio">					
					  <input  type='radio' name='template'  value="frontend" required
					  @if( $row['template'] =='frontend')  	checked	  @endif				  
					   /> 
					  Frontend
					</label> 
					<label class="radio">					
					  <input  type='radio' name='template'  value="backend" required
					   @if( $row['template'] =='backend')  	checked	  @endif				  
					   /> 
					  Backend
					</label> 					 
				  </div>
					
				  <div class="form-group  " >
					<label> Transparent Header </label>
					<label class="radio">					
					  <input  type='radio' name='transparent'  value="0" required
					  @if( $row['transparent'] =='0')  	checked	  @endif				  
					   /> 
					  No
					</label> 
					<label class="radio">					
					  <input  type='radio' name='transparent'  value="1" required
					   @if( $row['transparent'] =='1')  	checked	  @endif				  
					   /> 
					  Yes
					</label> 					 
				  </div>

				  <div class="form-group  " >
					<label> Set As Homepage ? </label>
					<label class="checkbox"><input  type='checkbox' name='default' 
 						@if($row['default'] ==1 ) checked  @endif	
					   value="1"	/> Yes  
					</lable>					 
				  </div> 				  			  
				  
			  <div class="form-group">
				
				<button type="submit" class="btn btn-primary ">  Submit </button>
				<a href="{{ url('core/pages')}}" class="btn btn-info"> Cancel </a>
				 
		
			  </div> 
			  </div>
			  </div>				  				  
				  		
			</div>

		 {!! Form::close() !!}
	</div>
</div>

<div id="form_div" class="hidden"></div> 
	
<script>
$('#title').focusout(function(){
    var title = $('#title').val().replace(/[����]/g,'a').replace(/[���]/g,'e').replace(/[����]/g,'o').replace(/[���]/g,'u').toLowerCase();
    document.getElementById('alias').value=title.replace(/\s/g, '');
    document.getElementById('filename').value=title.replace(/\s/g, '');
});
</script>
	
<script>
	
	function savePage() {
		$.post('{{URL::to('core/pages')}}/savepage', {
		   pageID: {{$row['pageID']}},
		   content: $('#content').val()
	       }, function (data) {
		   alert('Saved!');
	       });
	}
	  
	function preview(html) {
		var urlAction = '{{URL::to('preview')}}';
		var $form = $('<form target="_blank" method="POST" action="' + urlAction + '">');
		var input_id = $("<input>")
		.attr("type", "text")
		.attr("name", "id")
		.val({{$row['pageID']}});
		$form.append($(input_id));
		var input_html = $("<input>")
		.attr("type", "text")
		.attr("name", "html")
		.val(html);
		$form.append($(input_html));
		$form.submit();
		$form.remove();
	      }
	      
	function preview(html) {
            var form_div = $('#form_div');
            var urlAction = '{{URL::to('preview')}}';
            var $form = $('<form target="_blank" method="POST" action="' + urlAction + '">');
            var input_id = $("<input>")
            .attr("type", "text")
            .attr("name", "id")
            .val({{$row['pageID']}});
            $form.append($(input_id));
            var input_html = $("<input>")
            .attr("type", "text")
            .attr("name", "html")
            .val(html);
            $form.append($(input_html));
            form_div.empty();
            form_div.append($form);
            $form.submit();
        }
</script>
	
<script>
tinymce.init({
	selector: "#content",
	theme: "modern",
	skin: 'light',
	height : 610,
	valid_elements : '+*[*]',
	cleanup_on_startup: false,
	trim_span_elements: false,
	entity_encoding : "raw",
	verify_html: false,
	cleanup: false,
	convert_urls: false,
	document_base_url : '{{ URL::to('') }}/',
	visualblocks_default_state: true,
	content_css : ["{{ URL::to('') }}/variant/theme/css/bootstrap.css","{{ URL::to('') }}/variant/theme/css/animate.css","{{ URL::to('') }}/variant/theme/css/theme.css",'{{ URL::to('') }}/variant/theme/css/font-awesome.min.css'],
	plugins: [ 
	"advlist autolink link image lists charmap print hr anchor pagebreak", 
	"searchreplace wordcount visualblocks visualchars code insertdatetime nonbreaking", 
	"table contextmenu directionality emoticons paste textcolor filemanager fullscreen bootstrap save" 
	],
	bootstrapConfig: {
	    'imagesPath': '{{ URL::to('') }}/img/' // replace with your images folder path
	},
	image_advtab: true, 
	toolbar: "save previewbtn | undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image | print preview code fullscreen | bootstrap",
	save_enablewhendirty: false,
	save_onsavecallback: function (ed) {
	    $('#content').val(ed.getContent());
	    savePage();
	    //alert('Saved');
	},
	setup: function (ed) {
	    ed.addButton('previewbtn', {
		title : 'Preview',
		text: 'Preview',
		icon: 'mce-ico mce-i-preview',
		onclick : function() {
		    preview(ed.getContent());
		}
	     });
	    /*ed.on('init', function(args) {
		console.debug(args.target.id);
		ed.setContent($('#content_'+lang).val());
		setTimeout(function(){
		    ed.execCommand('mceFullScreen');
		}, 1000);
		
	    });*/
	},
	init_instance_callback: function (inst) { inst.execCommand('mceAutoResize'); }
  
});
</script>

<style type="text/css">
.note-editor .note-editable { height:500px;}
</style>			 	 
@stop