@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
<style>
.page-build {
    color: #fff !important;
    background: #ed4749 !important;
}
</style>
  <div class="page-content row">

	
	<div class="page-content-wrapper m-t">	 

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3></h5>
		<div class="sbox-tools" >

		</div>
	</div>
	<div class="sbox-content"> 	
	    <div class="toolbar-line ">
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('core/pages/update') }}" class="tips btn btn-sm btn-white"  title="{{ Lang::get('core.btn_create') }}">
			<i class="icon-plus-circle2 "></i>&nbsp;{{ Lang::get('core.btn_create') }}</a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="SximoDelete();" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_remove') }}">
			<i class="icon-remove4"></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
			@endif 		
			@if($access['is_excel'] ==1)
			<a href="{{ URL::to('core/pages/download') }}" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_download') }}">
			<i class="icon-file-download"></i>&nbsp;{{ Lang::get('core.btn_download') }} </a>
			@endif			
		 
		</div> 		

	
	
	 {!! Form::open(array('url'=>'core/pages/delete/', 'class'=>'form-horizontal' ,'id' =>'SximoTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> No </th>
				<th> <input type="checkbox" class="checkall" /></th>
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th class="hidden"></th>
				<th width="165" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
						
            @foreach ($rowData as $row)
                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $row->pageID }}" />  </td>									
				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 <td>	
					 	@if($field['field'] =='default')
					 		{!! $row->default == 1 ? '<i class="text-success fa fa-check-circle"></i>' : '<i class="text-danger fa fa-minus-circle"></i>'  !!}

					 	@else 
					 		{!! SiteHelpers::formatRows($row->{$field['field']},$field) !!}	
					 	@endif

					 						 
					 </td>
					 @endif					 
				 @endforeach
			         <td class="hidden">
				    <textarea id="{{$row->pageID}}">{{ $row->content }}</textarea>
				    <input type="text" name="title_{{$row->pageID}}" value="{{ $row->title }}" />
				 
				 </td>
				 <td>
					 	@if($access['is_detail'] ==1)
					 		@if($row->pageID == 1)
					 		<a href="{{ url()}}" target="_blank" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
					 		@else
							<a href="{{ url($row->alias)}}" target="_blank" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
							@endif
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ url('core/pages/update/'.$row->pageID.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						<a  onclick="configItem({{$row->pageID}})" class="tips btn btn-xs btn-white page-build" title="Build"><i class="fa fa-code"></i> <!--<span class="hidden-xs">Build</span>--></a>
						<div class="dropdown" style="display: inline;">
						    <button class="btn btn-xs btn-warning dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						      History
						      <span class="caret"></span>
						    </button>
						    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu1">
							@foreach(PageHelpers::history($row->pageID) as $history)
							    <li><textarea class="hidden" id="history_{{$history->id}}">{{ $history->content }}</textarea><a onclick="configHistory({{$row->pageID}},{{$history->id}})">{{$history->created_at}}</a></li>
							@endforeach
						      <!--<li><a href="#">Another action</a></li>
						      <li><a href="#">Something else here</a></li>
						      <li role="separator" class="divider"></li>
						      <li><a href="#">Separated link</a></li>-->
						    </ul>
						</div>
						@endif
												
					
				</td>				 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
	</div>
</div>	
	</div>	  
</div>

<div id="variant-builder"></div>

<div id="form_div" class="hidden"></div>  

<script>
$(document).ready(function(){

	$('.do-quick-search').click(function(){
		$('#SximoTable').attr('action','{{ URL::to("core/pages/multisearch")}}');
		$('#SximoTable').submit();
	});
	
});	
</script>
      
<script> 
function exit_builder() {
      $('#variant-builder *').remove();
      $("body").css("overflow", "auto");
}

function configItem(section) {  
      $.get('{{URL::to('core/pages')}}/variant/'+section, function (data) {
	  $('#variant-builder').html(data);
      });
}

function configHistory(section,history) {
    var c = confirm('This is a Backup, Saving will Overwrite the Page!');
    if (c == true) {
      $.get('{{URL::to('core/pages')}}/varianthistory/'+section+'/'+history, function (data) {
	    $('#variant-builder').html(data);
      });
    }
}

function savePage(page) {
      $.post('{{URL::to('core/pages')}}/savepage', {
	 pageID: page,
	 content: $('#'+page).val()
     }, function (data) {
	 //console.log(data);
     });
}

function preview(id,new_html) {
    var form_div = $('#form_div');
    var urlAction = '{{URL::to('preview')}}';
    var $form = $('<form target="_blank" method="POST" action="' + urlAction + '">');
    var input_id = $("<input>")
    .attr("type", "text")
    .attr("name", "id")
    .val(id);
    $form.append($(input_id));
    var input_html = $("<input>")
    .attr("type", "text")
    .attr("name", "html")
    .val(new_html);
    $form.append($(input_html));
    form_div.empty();
    form_div.append($form);
    $form.submit();
}
</script>		
@stop