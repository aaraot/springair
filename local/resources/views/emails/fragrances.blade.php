<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Olá,</h2>
<p>Acabámos de receber um contacto através dos nossos fluxos em www.springair.com.pt.</p>
<br>
<p>
    Nome: {{$wfragrance->name}}<br>
    E-mail: {{$wfragrance->email}}<br>
    Contacto: {{$wfragrance->phone}}<br>
    Mensagem: {{$wfragrance->message}}<br><br>
    Espaço: {{$wfragrance->space}}<br>
    Negócio: {{$wfragrance->business}}<br>
    Dimensões: {{$wfragrance->dimensions}}<br>
</p>
<br>
<p>
    Visita a <a href="http://springair.com.pt/wizardfragrancesmanagement/show/{{$wfragrance->id}}?return=">Área
        administrativa</a> para ver.
</p>

<br/>

<p>Obrigado,</p>
{{ CNF_APPNAME }}
</body>
</html>