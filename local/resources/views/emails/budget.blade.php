<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Olá,</h2>
<p>Acabámos de receber um contacto através dos nossos fluxos em www.springair.com.pt.</p>
<br>
<p>
    Nome: {{$budget->name}}<br>
    E-mail: {{$budget->email}}<br>
    Contacto: {{$budget->phone}}<br>
    Mensagem: {{$budget->message}}<br>
</p>
<br>
<p>
    Visita a <a href="http://springair.com.pt/budgetsmanagement/show/{{$budget->id}}?return=">Área administrativa</a>
    para ver.
</p>

<br/>

<p>Obrigado,</p>
{{ CNF_APPNAME }}
</body>
</html>