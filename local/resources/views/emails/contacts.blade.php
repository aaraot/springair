<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Olá,</h2>
<p>Acabámos de receber um contacto através dos nossos fluxos em www.springair.com.pt.</p>
<br>
<p>
    Nome: {{$contact->name}}<br>
    E-mail: {{$contact->email}}<br>
    Contacto: {{$contact->phone}}<br>
    Mensagem: {{$contact->message}}<br>
</p>
<br>
<p>
    Visita a <a href="http://springair.com.pt/contactsmanagement/show/{{$contact->id}}?return=">Área administrativa</a>
    para ver.
</p>

<br/>

<p>Obrigado,</p>
{{ CNF_APPNAME }}
</body>
</html>