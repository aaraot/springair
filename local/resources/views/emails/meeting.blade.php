<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Olá,</h2>
<p>Acabámos de receber um contacto através dos nossos fluxos em www.springair.com.pt.</p>
<br>
<p>
    Nome: {{$meeting->name}}<br>
    E-mail: {{$meeting->email}}<br>
    Contacto: {{$meeting->phone}}<br>
    Mensagem: {{$meeting->message}}<br><br>
    Data: {{$meeting->date}}<br>
    Hora: {{$meeting->hour}}<br>
</p>
<br>
<p>
    Visita a <a href="http://springair.com.pt/meetingsmanagement/show/{{$meeting->id}}?return=">Área administrativa</a>
    para ver.
</p>

<br/>

<p>Obrigado,</p>
{{ CNF_APPNAME }}
</body>
</html>