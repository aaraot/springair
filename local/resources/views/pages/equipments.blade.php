@if ($obj['isMobile'])
    @include('pages.mobile.equipments-page')
@else
    @include('pages.desktop.equipments-content')
@endif