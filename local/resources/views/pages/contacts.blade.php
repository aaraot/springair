@if ($obj['isMobile'])
    @include('pages.mobile.contacts-page')
@else
    @include('pages.desktop.contacts-content')
@endif