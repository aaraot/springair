@if ($obj['isMobile'])
    @include('pages.mobile.service-page')
@else
    @include('pages.desktop.service-content')
@endif