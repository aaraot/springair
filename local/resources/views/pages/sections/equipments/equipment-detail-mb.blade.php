<section class="page-section equipment--detail--mb">
    <div class="row">
        <div class="col-12 equipment--detail--image gray-bg"
             style="background: url(http://static.springair.com.pt/uploads/equipments/{{ $equipment->image_detail_mb }}) no-repeat center !important;background-size: cover !important;">
            <a href="javascript:void(0)" class="link return close">
                <img class="back" src="http://static.springair.com.pt/frontend/images/ic_back_black.svg" width="" height="" alt="ic_back">
            </a>
        </div>

        <div class="col-12 equipment--detail--menu black-bg">
            <a href="javascript:void(0)" class="link return left hidden">
                <img src="http://static.springair.com.pt/frontend/images/ic_next2.svg" width="" height="" alt="ic_next">
                <span>Tamanho</span>
            </a>

            <a href="javascript:void(0)" class="link return right float-right">
                <span>Tecnologia</span>
                <img src="http://static.springair.com.pt/frontend/images/ic_next2.svg" width="" height="" alt="ic_next">
            </a>
        </div>

        <div class="col-12 equipment--detail--content white-bg">
            @include('pages.sections.equipments.detail.size-mb')
            @include('pages.sections.equipments.detail.tech-mb')
            @include('pages.sections.equipments.detail.cover-mb')
            @include('pages.sections.equipments.detail.budget-mb')
        </div>
    </div>
</section>