<section id="eq-budget">
    <div class="row">
        <div class="col-md-12">
            <h1 class="title text-center w-100">{{ $equipment->name }}</h1>

            <h3 class="title equipment--title underline-none text-center w-100">Pedir Orçamento</h3>

            <div class="form">
                <div class="row">
                    <div class="col-md-12 text-center">
                        @include('pages.components.form-image', ['class' => 'img-fluid'])
                    </div>

                    <div class="col-md-12">
                        @include('pages.components.form', ['btn' => 'btn-form-submit', 'style' => ''])
                    </div>
                </div>
            </div>

            <div class="form-submitted">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img class="img-fluid" src="http://static.springair.com.pt/frontend/images/illustration-analysis.svg" width=""
                             height="250" alt="illustration-analysis">
                    </div>

                    <div class="col-md-12 text-center">
                        <h2 class="title">
                            {!! $copies[19]->text !!}
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>