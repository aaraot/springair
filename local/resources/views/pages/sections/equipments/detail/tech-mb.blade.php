<section id="eq-tech">
    <div class="row">
        <div class="col-md-12">
            <h1 class="title">{{ $equipment->name }}</h1>

            <h3 class="title equipment--title underline-none">Tecnologia</h3>
        </div>

        @foreach($techs as $key=>$tech)
            <div class="col-md-12 tech--item">
                <div class="row">
                    <div class="col-md-3 tech--item--image"
                         style="background: url(http://static.springair.com.pt{{ $tech->image }}) no-repeat center;"></div>

                    <div class="col-9">
                        <h4 class="title tech--title underline-none">{{ $tech->name }}</h4>
                    </div>

                    <div class="col-12 tech--item--specs">
                        <p class="text tech--text black-color">{{ $tech->description }}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>