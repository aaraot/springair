<section id="eq-cover">
    <div class="row">
        <div class="col-md-12">
            <h1 class="title">{{ $equipment->name }}</h1>

            <h3 class="title equipment--title underline-none">Cobertura (perfume até {{ $equipment->cover }}m²)</h3>

            <h4 class="title tech--title underline-none">{{ $cover->name }}</h4>

            <p>{{ $cover->description }}</p>
        </div>

        <div class="col-md-12 text-center">
            <ul class="nav nav-tabs">
                @if ($equipment->cover <= (int)'75')
                    <li class="nav-item">
                        <a class="nav-link active" id="one-division-tab" data-toggle="tab" href="#one-division"
                           role="tab" aria-controls="one-division" aria-selected="true">1 Divisão</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="two-division-tab" data-toggle="tab" href="#two-division"
                           role="tab" aria-controls="two-division" aria-selected="false">2 Divisões</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="three-division-tab" data-toggle="tab" href="#three-division"
                           role="tab" aria-controls="three-division" aria-selected="false">3+ Divisões</a>
                    </li>
                @elseif($equipment->cover > (int)'75' && $equipment->cover <= (int)'500')
                    <li class="nav-item">
                        <a class="nav-link active" id="omnia-tab" data-toggle="tab" href="#omnia"
                           role="tab" aria-controls="omnia" aria-selected="true">Omnia</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link active" id="cryptoscent-tab" data-toggle="tab" href="#cryptoscent"
                           role="tab" aria-controls="cryptoscent" aria-selected="true">Aeroporto</a>
                    </li>
                @endif
            </ul>

            <div class="tab-content" id="myTabContent">
                @if ($equipment->cover <= (int)'75')
                    <div class="tab-pane fade show active" id="one-division" role="tabpanel"
                         aria-labelledby="one-division-tab">
                        @include('pages.sections.equipments.detail.svg.one-room')
                    </div>
                    <div class="tab-pane fade" id="two-division" role="tabpanel" aria-labelledby="two-division-tab">
                        @include('pages.sections.equipments.detail.svg.two-room')
                    </div>
                    <div class="tab-pane fade" id="three-division" role="tabpanel" aria-labelledby="three-division-tab">
                        @include('pages.sections.equipments.detail.svg.three-room')
                    </div>
                @elseif($equipment->cover > (int)'75' && $equipment->cover <= (int)'500')
                    <div class="tab-pane fade show active" id="omnia" role="tabpanel" aria-labelledby="omnia-tab">
                        @include('pages.sections.equipments.detail.svg.omnia')
                    </div>
                @else
                    <div class="tab-pane fade show active" id="cryptoscent" role="tabpanel"
                         aria-labelledby="cryptoscent-tab">@include('pages.sections.equipments.detail.svg.cryptoscent')
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>