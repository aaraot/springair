<section id="eq-size">
    <div class="row">
        <div class="col-md-12 text-center logo">
            <img src="http://static.springair.com.pt/frontend/images/logo-black.svg" width="" height="" alt="Springair black logo">
        </div>

        <div class="col-md-12">
            <h1 class="title">{{ $equipment->name }}</h1>

            <h3 class="title equipment--title underline-none">Tamanho</h3>

            <p class="text equipment--text black-color">{{ $size->text }}</p>

            <div class="img--container">
                <img class="img-fluid" src="http://static.springair.com.pt/uploads/equipments/{{ $size->image }}" width="" height=""
                     alt="Equipment image">
            </div>
        </div>
    </div>
</section>