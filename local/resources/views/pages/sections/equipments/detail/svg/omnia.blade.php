<style>
    .box {
        top: 0;
        right: 0;
    }

    .omnia .room {
        top: 4px;
        left: 4px;
        width: 303px;
        height: 266px;
    }

    .omnia .room .box {
        top: 126px;
        right: 146px;
        transform: rotate(45deg);
    }

    .omnia .room--wave {
        top: 51px;
        left: 72px;
        width: 160px;
        height: 160px;
        border-radius: 100%;
        transform-origin: center center;
    }

    .omnia .room--wave:nth-child(3) {
        top: 1px;
        left: 22px;
        width: 260px;
        height: 260px;
        animation-delay: .1s;
    }

    .omnia .room--wave:nth-child(4) {
        top: -69px;
        left: -48px;
        width: 400px;
        height: 400px;
        animation-delay: 0s;
    }

    /* Reverse */
    .omnia .room--wave:nth-child(2).reverse {
        animation-delay: 0s;
    }

    .omnia .room--wave:nth-child(3).reverse {
        animation-delay: .1s;
    }

    .omnia .room--wave:nth-child(4).reverse {
        animation-delay: .2s;
    }
</style>

<div id="cover" class="omnia">
    <div class="room">
        <div class="box box--animation">
            <div class="inner-box"></div>
        </div>
        <div class="room--wave"></div>
        <div class="room--wave"></div>
        <div class="room--wave"></div>
    </div>

    <img src="http://static.springair.com.pt/frontend/images/blueprint_omnia.svg" width="" height="" alt="blueprint_omnia">
</div>