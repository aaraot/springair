<style>
    .two-room .box {
        top: 0;
        right: 0;
    }

    /* Top Room */
    .two-room .top-room {
        top: 6px;
        right: 6px;
        width: 102px;
        height: 113px;
    }

    .two-room .top-room--wave {
        top: 0;
        right: 0;
        width: 100px;
        height: 104px;
        border-radius: 0 0 0 100%;
        transform-origin: right top;
    }

    .two-room .top-room--wave:nth-child(3) {
        width: 150px;
        height: 150px;
        animation-delay: 0s !important;
    }

    /* Reverse */
    .two-room .top-room--wave.reverse {
        animation-delay: 0s !important;
    }

    .two-room .top-room--wave:nth-child(3).reverse {
        animation-delay: .1s !important;
    }

    /* Bottom Room */
    .two-room .bottom-room {
        top: 118px;
        left: 4px;
        width: 289px;
        height: 148px;
    }

    .two-room .bottom-room .box {
        right: 140px;
    }

    .two-room .bottom-room--wave {
        top: -70px;
        left: 70px;
        width: 150px;
        height: 150px;
        border-radius: 100%;
        transform-origin: center center;
    }

    .two-room .bottom-room--wave:nth-child(3) {
        top: -120px;
        left: 20px;
        width: 250px;
        height: 250px;
        animation-delay: .1s !important;
    }

    .two-room .bottom-room--wave:nth-child(4) {
        top: -195px;
        left: -55px;
        width: 400px;
        height: 400px;
        animation-delay: 0s !important;
    }

    /* Reverse */
    .two-room .bottom-room--wave:nth-child(2).reverse {
        animation-delay: 0s !important;
    }

    .two-room .bottom-room--wave:nth-child(3).reverse {
        animation-delay: .1s !important;
    }

    .two-room .bottom-room--wave:nth-child(4).reverse {
        animation-delay: .2s !important;
    }

    @media (max-width: 576px) {
        .two-room .top-room {
            top: 0;
            right: 0;
            width: 95px;
            height: 100px;
        }

        .two-room .box {
            top: 5px;
            right: 5px;
        }

        .two-room .bottom-room {
            top: 99px;
            left: 4px;
            width: 241px;
            height: 123px;
        }

        .two-room .bottom-room .box {
            top: 0;
            right: 113px;
        }
    }
</style>

<div id="cover" class="two-room">
    <div class="room top-room">
        <div class="box box--animation">
            <div class="inner-box"></div>
        </div>
        <div class="room--wave top-room--wave"></div>
        <div class="room--wave top-room--wave"></div>
    </div>

    <div class="room bottom-room">
        <div class="box box--animation">
            <div class="inner-box"></div>
        </div>
        <div class="room--wave bottom-room--wave"></div>
        <div class="room--wave bottom-room--wave"></div>
        <div class="room--wave bottom-room--wave"></div>
    </div>

    <img src="http://static.springair.com.pt/frontend/images/blueprint_2_room.svg" width="" height="" alt="blueprint_2_room">
</div>