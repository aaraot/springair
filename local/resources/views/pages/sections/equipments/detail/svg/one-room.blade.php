<style>
    .one-room .room {
        top: 6px;
        left: 6px;
        width: 288px;
        height: 184px;
    }

    .one-room .room .box {
        right: 140px;
    }

    .one-room .room--wave {
        top: -70px;
        left: 70px;
        width: 150px;
        height: 150px;
        border-radius: 100%;
        transform-origin: center center;
    }

    .one-room .room--wave:nth-child(3) {
        top: -145px;
        left: -5px;
        width: 300px;
        height: 300px;
        animation-delay: .1s !important;
    }

    .one-room .room--wave:nth-child(4) {
        top: -220px;
        left: -80px;
        width: 450px;
        height: 450px;
        animation-delay: 0s !important;
    }

    /* Reverse */
    .one-room .room--wave:nth-child(2).reverse {
        animation-delay: 0s !important;
    }

    .one-room .room--wave:nth-child(3).reverse {
        animation-delay: .1s !important;
    }

    .one-room .room--wave:nth-child(4).reverse {
        animation-delay: .2s !important;
    }

    @media (max-width: 576px) {
        .one-room .room {
            top: 0;
            left: 0;
            width: 250px;
            height: 160px;
        }

        .one-room .room .box {
            right: 118px;
        }
    }
</style>

<div id="cover" class="one-room">
    <div class="room">
        <div class="box box--animation">
            <div class="inner-box"></div>
        </div>
        <div class="room--wave"></div>
        <div class="room--wave"></div>
        <div class="room--wave"></div>
    </div>

    <img src="http://static.springair.com.pt/frontend/images/blueprint_1_room.svg" width="" height="" alt="blueprint_1_room">
</div>