<style>
    .three-room .box {
        top: 0;
        right: 0;
    }

    .three-room .top-room {
        top: 5px;
        right: 5px;
        width: 93px;
        height: 102px;
    }

    .three-room .top-room:nth-child(1) {
        left: 5px;
    }

    .three-room .top-room:nth-child(2) {
        left: 103px;
    }

    .three-room .top-room--wave {
        top: 0;
        right: 0;
        width: 90px;
        height: 94px;
        border-radius: 0 0 0 100%;
        transform-origin: right top;
    }

    .three-room .top-room--wave:nth-child(2) {
        width: 135px;
        height: 135px;
    }

    /* Reverse */
    .three-room .room--wave:nth-child(2).reverse {
        animation-delay: 0s;
    }

    .three-room .room--wave:nth-child(3).reverse {
        animation-delay: .1s;
    }

    .three-room .room--wave:nth-child(4).reverse {
        animation-delay: .2s;
    }

    @media (max-width: 576px) {
        .three-room .top-room {
            width: 77px;
            height: 84px;
        }

        .three-room .top-room:nth-child(2) {
            left: 87px;
        }
    }
</style>

<div id="cover" class="three-room">
    <div class="room top-room">
        <div class="box box--animation">
            <div class="inner-box"></div>
        </div>
        <div class="room--wave top-room--wave"></div>
        <div class="room--wave top-room--wave"></div>
    </div>

    <div class="room top-room">
        <div class="box box--animation">
            <div class="inner-box"></div>
        </div>
        <div class="room--wave top-room--wave"></div>
        <div class="room--wave top-room--wave"></div>
    </div>

    <div class="room top-room">
        <div class="box box--animation">
            <div class="inner-box"></div>
        </div>
        <div class="room--wave top-room--wave"></div>
        <div class="room--wave top-room--wave"></div>
    </div>

    <img src="http://static.springair.com.pt/frontend/images/blueprint_3_room.svg" width="" height="" alt="blueprint_3_room">
</div>