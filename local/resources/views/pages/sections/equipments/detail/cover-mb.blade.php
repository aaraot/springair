<section id="eq-cover">
    <div class="row">
        <div class="col-md-12">
            <h1 class="title">{{ $equipment->name }}</h1>

            <h3 class="title equipment--title underline-none">Cobertura (perfume até {{ $equipment->cover }}m²)</h3>

            <h4 class="title tech--title underline-none">{{ $cover->name }}</h4>

            <p>{{ $cover->description }}</p>
        </div>

        <div class="col-md-12 text-center">
            <div class="covers-list">
                @if ($equipment->cover <= (int)'75')
                    <div class="card">
                        <div class="card--header">1 Divisão</div>

                        <div class="card--content">
                            @include('pages.sections.equipments.detail.svg.one-room')
                        </div>
                    </div>
                    <div class="card">
                        <div class="card--header">2 Divisões</div>

                        <div class="card--content">
                            @include('pages.sections.equipments.detail.svg.two-room')
                        </div>
                    </div>
                    <div class="card">
                        <div class="card--header">3+ Divisões</div>

                        <div class="card--content">
                            @include('pages.sections.equipments.detail.svg.three-room')
                        </div>
                    </div>
                @elseif($equipment->cover > (int)'75' && $equipment->cover <= (int)'500')
                    <div class="card">
                        <div class="card--header">Omnia</div>

                        <div class="card--content">
                            @include('pages.sections.equipments.detail.svg.omnia')
                        </div>
                    </div>
                @else
                    <div class="card">
                        <div class="card--header">Aeroporto</div>

                        <div class="card--content">
                            @include('pages.sections.equipments.detail.svg.cryptoscent')
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>