<section id="eq-tech">
    <div class="row">
        <div class="col-md-12">
            <h1 class="title">{{ $equipment->name }}</h1>

            <h3 class="title equipment--title underline-none">Tecnologia</h3>
        </div>

        @foreach($techs as $key=>$tech)
            @if ($key % 2 == 0)
                <div class="col-md-12 tech--item">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="tech--item--image"
                                 style="background: url(http://static.springair.com.pt{{ $tech->image }}) no-repeat center;"></div>
                        </div>

                        <div class="col-md-9 tech--item--specs">
                            <h4 class="title tech--title underline-none">{{ $tech->name }}</h4>

                            <p class="text tech--text black-color">{{ $tech->description }}</p>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-md-12 tech--item">
                    <div class="row">
                        <div class="col-md-9 tech--item--specs">
                            <h4 class="title tech--title underline-none">{{ $tech->name }}</h4>

                            <p class="text tech--text black-color">{{ $tech->description }}</p>
                        </div>

                        <div class="col-md-3">
                            <div class="tech--item--image float-right"
                                 style="background: url(http://static.springair.com.pt{{ $tech->image }}) no-repeat center;"></div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</section>