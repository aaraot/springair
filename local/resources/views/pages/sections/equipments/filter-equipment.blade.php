@foreach($equipments as $key=>$equipment)
    @if ($key % 2 == 0)
        <section id="{{ str_slug($equipment->name) }}" class="page-section equipment">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 d-md-none equipment-overview"
                         style="background: url(http://static.springair.com.pt/uploads/equipments/{{ $equipment->image_mb }}) no-repeat center;"></div>

                    <div class="col-sm-12 col-md-6 equipment--info">
                        <h4 class="title equipment--title">
                            {{ $equipment->name }}
                            <span class="cover">(perfuma até {{ $equipment->cover }}m²)</span>
                        </h4>

                        <p class="text equipment--text black-color">
                            {{ $equipment->description }}
                        </p>

                        <div class="btn--container">
                            <button type="button" class="btn black-color see-demo" data-id="{{ $equipment->id }}"
                                    ripple="ripple">
                                <div class="btn--round black-bg" ripple="ripple"></div>
                                {!! $obj['copies'][26]->text !!}
                            </button>
                        </div>
                    </div>

                    <div class="d-xs-none d-sm-none col-4 equipment-overview"
                         style="background: url(http://static.springair.com.pt/uploads/equipments/{{ $equipment->image }}) no-repeat center;"></div>
                </div>
            </div>
        </section>
    @else
        <section id="{{ str_slug($equipment->name) }}" class="page-section equipment black-bg">
            <div class="divider top"></div>
            <div class="container">
                <div class="row">
                    {{-- Desktop --}}
                    <div class="d-xs-none d-sm-none col-4 equipment-overview"
                         style="background: url(http://static.springair.com.pt/uploads/equipments/{{ $equipment->image }}) no-repeat center;"></div>
                    {{-- Mobile --}}
                    <div class="col-sm-12 d-md-none equipment-overview"
                         style="background: url(http://static.springair.com.pt/uploads/equipments/{{ $equipment->image_mb }}) no-repeat center;"></div>

                    <div class="col-sm-12 col-md-6 equipment--info">
                        <h4 class="title equipment--title white-color">
                            {{ $equipment->name }}
                            <span class="cover white-color">(perfuma até {{ $equipment->cover }}m²)</span>
                        </h4>

                        <p class="text equipment--text white-color">
                            {{ $equipment->description }}
                        </p>

                        <div class="btn--container">
                            <button type="button" class="btn purple-color see-demo" data-id="{{ $equipment->id }}"
                                    ripple="ripple">
                                <div class="btn--round" ripple="ripple"></div>
                                {!! $obj['copies'][26]->text !!}
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            {{--@if(count($equipments) - 1 !== $key)--}}
            <div class="divider bottom"></div>
            {{--@endif--}}
        </section>
    @endif
@endforeach