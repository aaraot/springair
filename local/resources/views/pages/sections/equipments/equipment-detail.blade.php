<section class="page-section equipment--detail">
    <div class="row">
        <div class="col-3 equipment--detail--image gray-bg"
             style="background: url(http://static.springair.com.pt/uploads/equipments/{{ $equipment->image_detail }}) no-repeat center !important;background-size: cover !important;"></div>

        <div class="col-6 equipment--detail--content white-bg">
            @include('pages.sections.equipments.detail.size')
            @include('pages.sections.equipments.detail.tech')
            @include('pages.sections.equipments.detail.cover')
            @include('pages.sections.equipments.detail.budget')
        </div>

        <div class="col-3 equipment--detail--menu black-bg">
            <a href="javascript:void(0)" class="link return close">
                <img src="http://static.springair.com.pt/frontend/images/ic_back.svg" width="" height="" alt="ic_back">
                Voltar
            </a>

            <button class="iconbutton">
                <div class="scroll">
                    <div class="scroll__wheel"></div>
                </div>
            </button>

            <nav class="vertical-menu">
                <ul>
                    <li class="active">
                        <a href="#eq-size">1. Tamanho</a>
                    </li>
                    <li>
                        <a href="#eq-tech">2. Tecnologia</a>
                    </li>
                    <li>
                        <a href="#eq-cover">3. Cobertura</a>
                    </li>
                    <li>
                        <a href="#eq-budget">4. Orçamento</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <nav class="horizontal-menu">
        <a href="" class="back"></a>

        <a href="#eq-tech" class="next">
            <img src="http://static.springair.com.pt/frontend/images/ic_next.svg" width="" height="" alt="ic_next">
            Tecnologia
        </a>
    </nav>
</section>