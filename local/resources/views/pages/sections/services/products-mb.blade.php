<div class="col-md-12 text-center">
    <div class="covers-list">
        <div class="card">
            <div class="card--header">1 Divisão</div>

            <div class="card--content">
                @include('pages.sections.equipments.detail.svg.one-room')
            </div>
        </div>
        <div class="card">
            <div class="card--header">2 Divisão</div>

            <div class="card--content">
                @include('pages.sections.equipments.detail.svg.two-room')
            </div>
        </div>
        <div class="card">
            <div class="card--header">3+ Divisões</div>

            <div class="card--content">
                @include('pages.sections.equipments.detail.svg.three-room')
            </div>
        </div>
        <div class="card">
            <div class="card--header">Omnia</div>

            <div class="card--content">
                @include('pages.sections.equipments.detail.svg.omnia')
            </div>
        </div>
        <div class="card">
            <div class="card--header">Aeroporto</div>

            <div class="card--content">
                @include('pages.sections.services.cryptoscent')
            </div>
        </div>
    </div>
</div>