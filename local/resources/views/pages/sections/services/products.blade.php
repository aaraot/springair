<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link active" id="one-division-tab-{{ $service->id }}" data-toggle="tab"
           href="#one-division-{{ $service->id }}" role="tab" aria-controls="one-division-{{ $service->id }}"
           aria-selected="true">1 Divisão</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="two-division-tab-{{ $service->id }}" data-toggle="tab"
           href="#two-division-{{ $service->id }}" role="tab" aria-controls="two-division-{{ $service->id }}"
           aria-selected="false">2 Divisões</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="three-division-tab-{{ $service->id }}" data-toggle="tab"
           href="#three-division-{{ $service->id }}" role="tab" aria-controls="three-division-{{ $service->id }}"
           aria-selected="false">3+ Divisões</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="omnia-tab-{{ $service->id }}" data-toggle="tab" href="#omnia-{{ $service->id }}"
           role="tab" aria-controls="omnia-{{ $service->id }}" aria-selected="false">Omnia</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="crypto-tab-{{ $service->id }}" data-toggle="tab" href="#crypto-{{ $service->id }}"
           role="tab" aria-controls="crypto-{{ $service->id }}" aria-selected="false">Cryptoscent</a>
    </li>
</ul>

<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="one-division" role="tabpanel"
         aria-labelledby="one-division-tab">
        @include('pages.sections.equipments.detail.svg.one-room')
    </div>
    <div class="tab-pane fade" id="two-division-{{ $service->id }}" role="tabpanel"
         aria-labelledby="two-division-tab-{{ $service->id }}">
        @include('pages.sections.equipments.detail.svg.two-room')
    </div>
    <div class="tab-pane fade" id="three-division-{{ $service->id }}" role="tabpanel"
         aria-labelledby="three-division-tab-{{ $service->id }}">
        @include('pages.sections.equipments.detail.svg.three-room')
    </div>
    <div class="tab-pane fade" id="omnia-{{ $service->id }}" role="tabpanel"
         aria-labelledby="omnia-tab-{{ $service->id }}">
        @include('pages.sections.equipments.detail.svg.omnia')
    </div>
    <div class="tab-pane fade" id="crypto-{{ $service->id }}" role="tabpanel"
         aria-labelledby="crypto-tab-{{ $service->id }}">
        @include('pages.sections.services.cryptoscent')
    </div>
</div>