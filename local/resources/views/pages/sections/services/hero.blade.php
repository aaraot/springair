<div class="row">
    <div class="col-md-12">
        <h2 class="title section--title w-border">{{ $service->title }}</h2>
    </div>

    <div class="col-md-12">
        <p class="text text--gray">
            {!! $service->text !!}
        </p>
    </div>

    <div class="col-md-12 text-center">
        <label class="scroll-to">
            <a href="#service-details">Saber Mais</a>
        </label>

        <button class="iconbutton">
            <div class="scroll">
                <div class="scroll__wheel"></div>
            </div>
        </button>
    </div>

    <div class="col-md-12 text-center">
        <img class="img-fluid" src="http://static.springair.com.pt/uploads/services/{{ $service->hero_image }}" width="" height=""
             alt="{{ $service->title }}" style="margin-top: 80px">
    </div>
</div>