<div id="service-details" class="service-details">
    @foreach($obj['servicesHighlights'] as $servicesHighlight)
        @if ($servicesHighlight->service_id == $service->id)
            <div class="card opacity-0">
                <div class="image card--image"
                     style="background: url(http://static.springair.com.pt{{ $servicesHighlight->image }})"></div>

                <h5 class="title card--title">{{ $servicesHighlight->title }}</h5>

                <p class="text card--text">
                    {!! $servicesHighlight->description !!}
                </p>
            </div>
        @endif
    @endforeach
</div>