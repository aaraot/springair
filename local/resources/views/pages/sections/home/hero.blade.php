<div class="container">
    <h1 class="title {{ $obj['pages'][0]->style == 1 ? 'black' : 'white' }}">{!! $obj['copies'][0]->text !!}</h1>
</div>

<section class="clients-list-container">
    <h3 class="title {{ $obj['pages'][0]->style == 1 ? 'black' : 'white' }}">Clientes</h3>

    <div class="clients-list">
        @foreach($obj['clients'] as $client)
            <a href="{{ $client->link }}" target="_blank">
                <div class="clients-list--item"
                     style="background: url(http://static.springair.com.pt/uploads/clients/{{ $client->image }})"></div>
            </a>
        @endforeach
    </div>
</section>