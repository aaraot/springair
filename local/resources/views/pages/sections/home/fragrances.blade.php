<div class="fragrances" style="transform: translateX(0);">
    @include('pages.components.bubble-animation', ['direction' => 'down'])

    <div class="banner parallax" style="background: url(http://static.springair.com.pt/uploads/pages/{{ $obj['images'][0]->image }})">
        <div class="overlay"></div>

        <div class="container position-relative">
            <h2 class="title section--title w-border">{!! $obj['copies'][2]->text !!}</h2>

            <p class="text">{!! $obj['copies'][3]->text !!}</p>
        </div>
    </div>

    <div class="btn-container">
        <button type="button" class="btn flatten" onclick="$homepage.showWizard($(this), '.fragrances')">
            <div class="ripple--container">
                <div ripple="ripple"></div>
            </div>
            {!! $obj['copies'][22]->text !!}
        </button>
    </div>

    <div class="image" style="background: url(http://static.springair.com.pt/uploads/pages/{{ $obj['images'][1]->image }});"></div>
</div>

<div class="fragrances wizard--fragrances" style="transform: translateX(100%);visibility: hidden;">
    <canvas id="confetti-1" class="confetti-container"></canvas>

    <div class="container">
        @include('pages.components.fragrances-wizard')
    </div>
</div>