<h2 class="title section--title">{!! $obj['copies'][4]->text !!}</h2>

<div class="text-box">
    <p class="text box--text">
        <span class="box--text-hifen"></span>
        {!! $obj['copies'][5]->text !!}
    </p>
</div>

<div class="btn-container">
    <button type="button" class="btn black inline flatten btn--goTo" data-link="equipamentos">
        <div class="ripple--container">
            <div ripple="ripple"></div>
        </div>
        {!! $obj['copies'][23]->text !!}
    </button>

    {{--<button type="button" class="btn inline flatten">--}}
        {{--<div class="ripple--container">--}}
            {{--<div ripple="ripple"></div>--}}
        {{--</div>--}}
        {{--{!! $obj['copies'][24]->text !!}--}}
    {{--</button>--}}
</div>

<div class="image" style="background: url(http://static.springair.com.pt/uploads/pages/{{ $obj['images'][2]->image }})"></div>