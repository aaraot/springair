<div class="services">
    <h2 class="title section--title">{!! $obj['copies'][1]->text !!}</h2>

    <div class="row">
        <div class="col-md-12">
            <div class="services-list">
                @foreach($obj['services'] as $key=>$service)
                    <div class="card">
                        <div class="card--header">
                            <div class="image card--image"
                                 style="background: url(http://static.springair.com.pt/uploads/services/{{ $service->card_image }})"></div>
                        </div>

                        <div class="card--content">
                            <h5 class="title card--title">{{ $service->title }}</h5>

                            <p class="text card--text">{{ $service->description }}</p>

                            <a href="javascript:void(0)" data-link="{{ str_slug($service->title) }}"
                               class="link card--link">Descobrir mais</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row" style="margin: 32px">
        <div class="col-md-12 text-center">
            <button type="button" class="btn flatten" onclick="$homepage.showWizard($(this), '.services')">
                <div class="ripple--container">
                    <div ripple="ripple"></div>
                </div>
                {!! $obj['copies'][21]->text !!}
            </button>
        </div>
    </div>
</div>

<div class="services wizard--services" style="transform: translateX(100%);visibility: hidden;">
    <canvas id="confetti-2" class="confetti-container"></canvas>

    <div class="container">
        @include('pages.components.meeting-wizard')
    </div>
</div>