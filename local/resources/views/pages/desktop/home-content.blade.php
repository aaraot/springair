@extends('layouts.springair.app')

@section('css')
@endsection

@section('content')
    <!-- pages stack -->
    <div class="pages-stack">
        <!-- page -->
        <div class="page" id="home">
            <div class="scrollbar-macosx">
                <section id="home-hero"
                         class="page-section parallax {{ $obj['pages'][0]->style == 1 ? 'black' : 'white' }}"
                         style="background: url(http://static.springair.com.pt/uploads/pages/{{ $obj['pages'][0]->hero_image }});">
                    @include('pages.sections.home.hero')
                </section>

                <section id="home-services" class="page-section">
                    @include('pages.sections.home.services')
                </section>

                <section id="home-fragrances" class="page-section">
                    @include('pages.sections.home.fragrances')
                </section>

                <section id="home-products" class="page-section">
                    @include('pages.sections.home.products')
                </section>

                @include('pages.components.quotes')
            </div>
        </div>
        <!-- /page -->

        <div class="page equipments" id="equipamentos">
            <div class="scrollbar-macosx">
                @include('pages.equipments')
            </div>
        </div>

        @foreach($obj['services'] as $service)
            <div class="page services" id="{{ str_slug($service->title) }}">
                <div class="scrollbar-macosx">
                    @include('pages.service', ['service' => $service])
                </div>
            </div>
        @endforeach

        <div class="page about" id="sobre-nos">
            <div class="scrollbar-macosx">
                @include('pages.about')
            </div>
        </div>

        <div class="page contacts" id="contactos">
            <div class="scrollbar-macosx">
                @include('pages.contacts')
            </div>
        </div>
    </div>
    <!-- /pages-stack -->
@endsection

@section('js')
@endsection