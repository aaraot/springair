<section class="page-section form">
    <h2 class="title contacts--title">{!! $obj['copies'][10]->text !!}</h2>

    <div class="row" style="margin: 48px 0 16px 0">
        <div class="col-md-12 text-center">
            @include('pages.components.form-image', ['class' => ''])
        </div>
    </div>

    <div class="row">
        <div class="col-xs-10 col-md-6 m-auto">
            @include('pages.components.form', ['btn' => '', 'style' => 'margin-top: 32px'])

            <div class="form-submitted">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="http://static.springair.com.pt/frontend/images/illustration-analysis.svg" width="" height="250"
                             alt="illustration-analysis">
                    </div>

                    <div class="col-md-12 text-center">
                        <h2 class="title contacts--title">
                            {!! $obj['copies'][20]->text !!}
                        </h2>
                    </div>

                    <div class="col-md-12 text-center" style="margin-top: 30px">
                        <button type="button" class="btn btn--goTo" data-link="home" ripple="ripple">
                            Homepage
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 text-center" style="margin: 48px 0">
            <span class="text text--gray">{!! $obj['copies'][11]->text !!}</span>
        </div>

        <div class="col-12 contacts--container">
            <h2 class="title contacts--title" style="margin-bottom: 34px">{!! $obj['copies'][12]->text !!}</h2>

            <span class="title contacts--title contact" title="Ligue-nos">
                <a class="link contacts--link" href="tel:{{ CNF_PHONE }}">{{ CNF_PHONE }}</a>
            </span>
            <span class="title contacts--title contact" title="Contacte-nos">
                <a class="link contacts--link" href="mailto:{{ CNF_EMAIL }}">{{ CNF_EMAIL }}</a>
            </span>

            <div class="pages-nav__item pages-nav__item--social" style="margin: 20px auto 48px">
                @foreach($obj['socials'] as $social)
                    <a class="link link--social link--faded" href="{{ $social->link }}" target="_blank">
                        <i class="fa fa-{{ $social->icon }}"></i>
                        <span class="text-hidden">{{ $social->name }}</span>
                    </a>
                @endforeach
            </div>
        </div>

        <div class="col-12">
            <span class="title contacts--title">{!! $obj['copies'][13]->text !!}</span>
            <span class="title contacts--title contact">{!! $obj['copies'][14]->text !!}</span>
        </div>

        <div class="col-12 p-0 map--container" style="margin-top: 56px">
            <div id="map" style="width: 100%;height: 500px"></div>
        </div>
    </div>
</section>