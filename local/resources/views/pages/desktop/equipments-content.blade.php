<section id="equipments-hero" class="page-section hero parallax"
         style="background: url(http://static.springair.com.pt/uploads/pages/{{ $obj['pages'][1]->hero_image }});">
    <div class="overlay"></div>
    @include('pages.components.bubble-animation')

    <div class="search--container">
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            @foreach($obj['categories'] as $category)
                <a class="dropdown-item" href="#" data-filter="{{ $category->id }}">{{ $category->name }}</a>
            @endforeach
        </div>

        <select class="equipment-categories" name="categories[]" multiple="multiple">
            @foreach($obj['categories'] as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
    </div>
</section>

<section class="section--title--container">
    <div class="container">
        <h2 class="title section--title">Equipamentos Spring Air</h2>
    </div>
</section>

<section id="equipments">
    @include('pages.sections.equipments.filter-equipment', ['equipments' => $obj['equipments']])
</section>

<section class="equipment--detail--container"></section>

@include('pages.components.quotes')