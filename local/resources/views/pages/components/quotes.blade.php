<section class="quotes--container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="title section--title w-border">{!! $obj['copies'][15]->text !!}</h2>
        </div>

        <div class="col-md-12">
            <p class="text text--gray">
                {!! $obj['copies'][16]->text !!}
            </p>
        </div>

        <div class="col-md-12 text-center" style="margin-top: 30px">
            <button type="button" class="btn flatten btn--goTo" data-link="contactos">
                <div class="ripple--container">
                    <div ripple="ripple"></div>
                </div>
                {!! $obj['copies'][25]->text !!}
            </button>
        </div>

        <div class="col-md-12" style="margin-top: 80px">
            <div class="card-stack--container">
                <div class="card-stack">
                    <ul class="card-list">
                        @foreach($obj['quotes'] as $quote)
                            <li id="{{ $quote->id }}" class="quote">
                                <div class="quote--container">
                                    <div class="quote--content">
                                        <img class="avatar"
                                             src="http://static.springair.com.pt/uploads/quotes/{{ $quote->image }}"
                                             width="" height="" alt="">

                                        <img class="quote-img"
                                             src="http://static.springair.com.pt/frontend/images/quote.svg" width=""
                                             height="" alt="quote">

                                        <p class="text quote--text">{{ $quote->text }}</p>
                                    </div>

                                    <div class="quote--footer">
                                        <img src="http://static.springair.com.pt/frontend/images/ic_arrow_quotes.svg"
                                             title="Prev" class="prev">

                                        <div>
                                            <span class="quote--client">{{ $quote->name }}</span>
                                            <span class="quote--client-role">{{ $quote->role }} @ {{ $quote->company }}</span>
                                        </div>

                                        <img src="http://static.springair.com.pt/frontend/images/ic_arrow_quotes.svg"
                                             title="Next" class="next">
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="card-pagination">
                    <ul>
                        @foreach($obj['quotes'] as $index => $value)
                            @if($index === 0)
                                <li class="active"></li>
                            @else
                                <li></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>