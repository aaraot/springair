<div class="final-step hidden">
    <div class="row">
        <div class="col-md-12 text-center">
            <h2 class="title wizard-step--title">
                Como podemos entrar em contacto consigo?
            </h2>

            <span class="text wizard-step--text">
                (Confirme os dados da reunião e insira os seus dados para concluir a marcação)
            </span>
        </div>
    </div>

    <div class="row">
        <span class="demonstration">Demonstração 1h30</span>

        <span class="demonstration-date text-center"></span>
    </div>

    <div class="row" style="margin: 24px 0">
        <div class="col-md-12 text-center">
            @include('pages.components.form-image', ['class' => ''])
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @include('pages.components.form', ['btn' => 'btn-final-step', 'style' => 'margin-top: 16px; margin-bottom: 48px'])
        </div>
    </div>
</div>

<div class="final-step hidden text-center">
    <div class="row">
        <div class="col-md-12">
            <img src="http://static.springair.com.pt/frontend/images/illustration-calendar.svg" width="" height=""
                 alt="illustration-calendar" style="margin: 30px 0;">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2 class="title wizard-step--title">
                {!! $obj['copies'][17]->text !!}
            </h2>
        </div>
    </div>

    <div class="row" style="margin-top: 8px">
        <div class="col-md-12">
            <button type="button" class="btn black flatten" style="width: 180px" onclick="$homepage.closeWizard()"
                    ripple="ripple">
                Homepage
            </button>
        </div>
    </div>
</div>