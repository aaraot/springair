<div class="row">
    <div class="col-md-12 text-center">
        <h2 class="title wizard-step--title">A que horas pretende reunir?</h2>
        <span class="text wizard-step--text">(escolha a baixo o horário pretendido)</span>
    </div>
</div>

<div class="row wizard-step-content">
    <div class="col-md-12 text-center">
        <div id='calendar2'></div>
    </div>

    <div class="col-md-12 text-center">
        <span class="demonstration">Demonstração 1h30</span>
    </div>

    <div class="col-md-12 text-center">
        <input type="text" id="timepicker" class="form-input text-center" placeholder="Hora Início"/>
    </div>

    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-next-step loose flatten" ripple="ripple">
            Confirmar
        </button>
    </div>
</div>