<div class="row">
    <div class="col-md-12 text-center">
        <h2 class="title wizard-step--title">Em que dia pretende reunir connosco?</h2>
        <span class="text wizard-step--text">(escolha o seu dia preferido para reunir)</span>
    </div>
</div>

<div class="row wizard-step-content">
    <div class="col-md-12 text-center">
        <div id='calendar'></div>
    </div>

    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-next-step loose flatten" ripple="ripple">
            Confirmar
        </button>
    </div>
</div>