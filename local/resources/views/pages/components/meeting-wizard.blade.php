<div class="wizard">
    <ul class="wizard-steps text-center" role="tablist">
        <li class="active" role="tab">
            <span>1</span>
            <div class="checkmark draw"></div>
        </li>
        <li role="tab">
            <span>2</span>
            <div class="checkmark draw"></div>
        </li>
        <li role="tab">
            <span>3</span>
            <div class="checkmark draw"></div>
        </li>
    </ul>

    <div class="wizard-content">
        <div class="wizard-pane active" role="tabpanel">
            <div class="scrollbar-macosx">
                @include('pages.components.meeting-wizard-steps.step-one')
            </div>
        </div>

        <div class="wizard-pane" role="tabpanel">
            @include('pages.components.meeting-wizard-steps.step-two')
        </div>

        <div class="wizard-pane" role="tabpanel">
            <div class="scrollbar-macosx">
                @include('pages.components.meeting-wizard-steps.step-three')
            </div>
        </div>
    </div>
</div>