<nav id="cd-vertical-nav">
    <ul>
        @foreach($steps as $index => $value)
            <li>
                <div id="{{ $index }}" class="@if ($index === 0) is-selected @endif">
                    <span class="cd-dot"></span>
                    <span class="cd-label">{{ $value }}</span>
                </div>
            </li>
        @endforeach
    </ul>
</nav>