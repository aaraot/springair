<div class="row">
    <div class="col-md-12 text-center">
        <h2 class="title wizard-step--title">Qual o tipo do seu negócio?</h2>
        <span class="text wizard-step--text">(escolha um tipo de negócio embaixo)</span>
    </div>
</div>

<div class="row wizard-step-content">
    <?php $row = 0; ?>
    @foreach($obj['businesses'] as $key=>$business)
        @if ($row == 0)
            <div class="col-md-12 text-center" style="margin-bottom: 30px">
                @endif
                <div class="wizard-card">
                    <div class="wizard-card--image-container">
                        <img src="http://static.springair.com.pt/uploads/businesses/{{ $business->image }}"
                             class="img" width="" height="" alt="{{ $business->name }}">
                        <img src="http://static.springair.com.pt/uploads/businesses/{{ $business->hover }}"
                             class="img--hover hidden" width="" height="" alt="{{ $business->name }}">
                    </div>

                    <div class="title wizard-card--title">{{ $business->name }}</div>
                </div>
                @if ($row == 2 || $key === count($obj['businesses']) - 1)
            </div>
            <?php $row = 0; ?>
        @else
            <?php $row++; ?>
        @endif
    @endforeach
</div>