<div class="row">
    <div class="col-md-12 text-center">
        <h2 class="title wizard-step--title"></h2>
        <span class="text wizard-step--text">(escolha a dimensão do seu espaço em baixo)</span>
    </div>
</div>

<div class="row wizard-step-content">
    <div class="col-md-12 text-center">
        <div class="wizard-card no-pointer" data-next="false">
            <div class="wizard-card--image-container">
                <img src="" width="" height="" alt="">
            </div>

            <div class="title wizard-card--title"></div>
        </div>
    </div>

    <div class="col-md-12 text-center" style="margin-top: 30px">
        <span class="wizard-range-slider--value">menos de 45m2</span>

        <input class="asRange" type="range" min="45" max="500" name="points" step="10" value="0"/>

        <div class="wizard-range-slider--minMax-container">
            <span class="wizard-range-slider--min">menos de 45m2</span>
            <span class="wizard-range-slider--max">mais de 500m2</span>
        </div>
    </div>

    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-next-step loose flatten" ripple="ripple">
            Confirmar
        </button>
    </div>
</div>