<div class="row" style="margin-bottom: 40px;">
    <div class="col-md-12 text-center">
        <h2 class="title wizard-step--title">Quais os valores do seu negócio?</h2>
        <span class="text wizard-step--text">
            (escolha uma ou mais de entre as opções em baixo. Isto ajuda-nos a encontrar a fragrância perfeita para o seu negócio)
        </span>
    </div>
</div>

<?php $row = 0; ?>
@foreach($obj['values'] as $key=>$value)
    @if ($row == 0)
        <div class="row features">
            <div class="col-md-12">
                <div class="feature-list">
    @endif
                    <div class="feature-list--item">
                        <div class="feature-list--item--icon">
                            <div class="horizontal"></div>
                            <div class="vertical"></div>
                        </div>

                        <span class="feature-list--item--text">{{ $value->name }}</span>
                    </div>
    @if ($row == 3 || $key === count($obj['values']) - 1)
                </div>
            </div>
        </div>
        <?php $row = 0; ?>
    @else
        <?php $row++; ?>
    @endif
@endforeach

<div class="row">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-next-step loose flatten" ripple="ripple">
            Confirmar
        </button>
    </div>
</div>