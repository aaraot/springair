<div class="row">
    <div class="col-md-12 text-center">
        <h2 class="title wizard-step--title">Para que espaço precisa de informações?</h2>
        <span class="text wizard-step--text">(escolha um tipo de espaço embaixo)</span>
    </div>
</div>

<div class="row wizard-step-content">
    <div class="col-md-12 text-center">
        @foreach($obj['spaces'] as $space)
            <div class="wizard-card" data-goTo="@if($space->goTo !== 0){{$space->goTo}}@endif">
                <div class="wizard-card--image-container">
                    <img src="http://static.springair.com.pt/uploads/spaces/{{ $space->image }}" class="img" width=""
                         height="" alt="{{ $space->name }}">
                    <img src="http://static.springair.com.pt/uploads/spaces/{{ $space->hover }}" class="img--hover hidden" width=""
                         height="" alt="{{ $space->name }}">
                </div>

                <div class="title wizard-card--title">{{ $space->name }}</div>
            </div>
        @endforeach
    </div>
</div>