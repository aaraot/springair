<div class="final-step hiddden">
    <div class="row">
        <div class="col-md-12 text-center">
            <h2 class="title wizard-step--title">
                Vamos analisar o seu pedido e entrar em contacto consigo em breve.
            </h2>

            <span class="text wizard-step--text">(já só falta confirmar tudo e inserir os seus detalhes)</span>
        </div>
    </div>

    <div class="row" style="margin: 24px 0">
        <div class="col-md-12 text-center">
            @include('pages.components.form-image', ['class' => ''])
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @include('pages.components.form', ['btn' => 'btn-final-step', 'style' => 'margin-bottom: 48px'])
        </div>
    </div>
</div>

<div class="final-step hidden text-center">
    <div class="row">
        <div class="col-md-12">
            <img src="http://static.springair.com.pt/frontend/images/illustration-brewing.svg" width="" height=""
                 alt="illustration-brewing" style="margin: 30px 0;">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2 class="title wizard-step--title">
                {!! $obj['copies'][18]->text !!}
            </h2>
        </div>
    </div>

    <div class="row" style="margin-top: 48px">
        <div class="col-sm-6 col-md-6 text-right">
            <button type="button" class="btn flatten" style="width: 180px"
                    onclick="$homepage.closeWizard(); setTimeout(function() {$homepage.showWizard($('.services .btn'), '.services');}, 1000)"
                    ripple="ripple">
                Agendar Reunião
            </button>
        </div>

        <div class="col-sm-6 col-md-6 text-left">
            <button type="button" class="btn black flatten" style="width: 180px" onclick="$homepage.closeWizard()"
                    ripple="ripple">
                Homepage
            </button>
        </div>
    </div>
</div>