<form>
    <div class="row">
        <div class="col-md-6">
            <div class="mat-div">
                <label for="name" class="form-label mat-label">Nome</label>
                <input type="text" class="form-input mat-input" id="name" name="name">
            </div>
        </div>

        <div class="col-md-6">
            <div class="mat-div">
                <label for="email" class="form-label mat-label">E-mail</label>
                <input type="email" class="form-input mat-input" id="email" name="email">
            </div>
        </div>

        <div class="col-md-6">
            <div class="mat-div">
                <label for="phone" class="form-label mat-label">Telemóvel</label>
                <input type="phone" class="form-input mat-input" id="phone" name="phone">
            </div>
        </div>

        <div class="col-md-6">
            <div class="mat-div">
                <label for="message" class="form-label mat-label">Mensagem</label>
                <input type="text" class="form-input mat-input" id="message">
            </div>
        </div>

        <div class="col-md-12 text-center" style="{{ $style }}">
            <button type="submit" class="btn {{ $btn }} flatten" ripple="ripple">
                Enviar Pedido
            </button>
        </div>
    </div>
</form>