<!-- pages stack -->
<div class="pages-stack">
    <!-- page -->
    <div class="page equipments" id="equipamentos">
        <div class="scrollbar-macosx">
            @include('pages.desktop.equipments-content')
        </div>
    </div>
    <!-- /page -->
</div>
<!-- /pages-stack -->