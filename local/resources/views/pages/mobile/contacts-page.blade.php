@extends('layouts.springair.app')

@section('css')
@endsection

@section('content')
    <!-- pages stack -->
    <div class="pages-stack">
        <!-- page -->
        <div class="page contacts" id="contactos">
            <div class="scrollbar-macosx">
                @include('pages.desktop.contacts-content')
            </div>
        </div>
        <!-- /page -->
    </div>
    <!-- /pages-stack -->
@endsection

@section('js')
@endsection