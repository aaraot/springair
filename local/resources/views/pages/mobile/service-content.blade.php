<!-- pages stack -->
<div class="pages-stack">
    <!-- page -->
    @foreach($obj['services'] as $service)
        @if ($obj['service_id'] === $service->id)
            <div class="page services" id="{{ str_slug($service->title) }}">
                <div class="scrollbar-macosx">
                    @include('pages.desktop.service-content')
                </div>
            </div>
        @endif
    @endforeach
<!-- /page -->
</div>
<!-- /pages-stack -->