@extends('layouts.springair.app')

@section('css')
@endsection

@section('content')
    <!-- pages stack -->
    <div class="pages-stack">
        <!-- page -->
        @foreach($obj['services'] as $service)
            @if (str_contains($obj['page']['title'], $service->title))
                <div class="page services" id="{{ str_slug($service->title) }}">
                    <div class="scrollbar-macosx">
                        @include('pages.desktop.service-content')
                    </div>
                </div>
            @endif
        @endforeach
        <!-- /page -->
        </div>
    <!-- /pages-stack -->
@endsection

@section('js')
@endsection