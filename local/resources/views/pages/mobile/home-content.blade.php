<!-- pages stack -->
<div class="pages-stack">
    <!-- page -->
    <div class="page" id="home">
        <div class="scrollbar-macosx">
            <section id="home-hero"
                     class="page-section parallax {{ $obj['pages'][0]->style == 1 ? 'black' : 'white' }}"
                     style="background: url(http://static.springair.com.pt/uploads/pages/{{ $obj['pages'][0]->hero_image }});">
                @include('pages.sections.home.hero')
            </section>

            <section id="home-services" class="page-section">
                @include('pages.sections.home.services')
            </section>

            <section id="home-fragrances" class="page-section">
                @include('pages.sections.home.fragrances')
            </section>

            <section id="home-products" class="page-section">
                @include('pages.sections.home.products')
            </section>

            @include('pages.components.quotes')
        </div>
    </div>
    <!-- /page -->
</div>
<!-- /pages-stack -->