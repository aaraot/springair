@if ($obj['isMobile'])
    @include('pages.mobile.home-page')
@else
    @include('pages.desktop.home-content')
@endif
