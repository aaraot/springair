<!DOCTYPE html>
<html lang="pt" class="no-js">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta charset="utf-8">
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">
    <meta name="google-site-verification" content=""/>
    <meta name="robots" content="index">
    <meta name="author" content="Spring Air">
    <meta name="designer" content="Bruno Charters">
    <meta name="rating" content="general">
    <meta name="distribution" content="global">
    <meta name="copyright" content="Spring Air">
    <meta name="description" content="{{ $obj['page']['description'] }}">
    <meta name="image" content="http://static.springair.com.pt/frontend/images/facebook.png">
    <meta name="keywords" content="{{ $obj['page']['keywords'] }}">
    <link rel="canonical" href=""/>

    <!-- Schema.org for Google -->
    <meta itemprop="name" content="{{ $obj['page']['title'] }}">
    <meta itemprop="description" content="{{ $obj['page']['description'] }}">
    <meta itemprop="image" content="http://static.springair.com.pt/frontend/images/facebook.png">

    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="{{ $obj['page']['title'] }}">
    <meta name="og:description" content="{{ $obj['page']['description'] }}">
    <meta name="og:image" content="http://static.springair.com.pt/frontend/images/facebook.png">
    <meta name="og:url" content="springair.mediaweb.pt">
    <meta name="og:site_name" content="Spring Air">
    <meta name="og:locale" content="pt_PT">
    <meta name="og:type" content="website">

    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ $obj['page']['title'] }}">
    <meta name="twitter:description" content="{{ $obj['page']['description'] }}">
    <meta name="twitter:site" content="https://twitter.com/SpringairPT">
    <meta name="twitter:creator" content="@SpringairPT">
    <meta name="twitter:image:src" content="http://static.springair.com.pt/frontend/images/twitter.png">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="http://static.springair.com.pt/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="http://static.springair.com.pt/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="http://static.springair.com.pt/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="http://static.springair.com.pt/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="http://static.springair.com.pt/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="http://static.springair.com.pt/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="http://static.springair.com.pt/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="http://static.springair.com.pt/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="http://static.springair.com.pt/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"
          href="http://static.springair.com.pt/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="16x16" href="http://static.springair.com.pt/favicons/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="32x32" href="http://static.springair.com.pt/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="http://static.springair.com.pt/favicons/favicon-96x96.png">
    <link rel="alternate icon" type="image/png" href="{{ url('/favicon.ico') }}">
    <meta name="msapplication-TileImage" content="http://static.springair.com.pt/favicon.ico">

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#bf9b30">
    <meta name="msapplication-navbutton-color" content="#bf9b30">
    <meta name="msapplication-TileColor" content="#bf9b30">
    <meta name="apple-mobile-web-app-status-bar-style" content="#bf9b30">

    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <!-- MANIFEST -->
    <link rel="manifest" href="http://static.springair.com.pt/favicons/manifest.json') }}">

    <title>{{ $obj['page']['title'] }}</title>

    <!-- Fonts -->
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/fira_sans/fira-sans-v8-latin-200.woff2"
          as="font">
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/fira_sans/fira-sans-v8-latin-300.woff2"
          as="font">
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/fira_sans/fira-sans-v8-latin-500.woff2"
          as="font">
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/fira_sans/fira-sans-v8-latin-600.woff2"
          as="font">
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/fira_sans/fira-sans-v8-latin-800.woff2"
          as="font">
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/lato/lato-v14-latin-regular.woff2" as="font">
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/lato/lato-v14-latin-700.woff2" as="font">
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/lato/lato-v14-latin-900.woff2" as="font">
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/lora/lora-v12-latin-regular.woff2" as="font">
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/muli/muli-v12-latin-300.woff2" as="font">
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/muli/muli-v12-latin-600.woff2" as="font">
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/muli/muli-v12-latin-800.woff2" as="font">
    <link rel="prefetch" href="http://static.springair.com.pt/build/fonts/muli/muli-v12-latin-900.woff2" as="font">

    <!-- Libraries -->
    <link rel="stylesheet" href="{{ asset('frontend/libraries/font-awesome-4.7.0/css/font-awesome.min.css') }}">

    <!-- Frameworks -->
    <link rel="stylesheet"
          href="http://static.springair.com.pt/frontend/frameworks/bootstrap-4.0.0-beta.2/css/bootstrap.min.css">

    <!-- Loading Screen -->
    <style>
        body {
            height: 100vh;
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            background: #f6f6f6;
            scroll-behavior: smooth;
            overflow: hidden;
        }

        #progress {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 5px;
            z-index: 2;
        }

        #progress.animate {
            animation: fadeOut 1s ease 7s both;
        }

        #progress.loading {
            animation: fadeOut 1s ease 2s both;
        }

        #progress .bar {
            background-color: #bf9b30;
            width: 0%;
            height: 5px;
        }

        #progress.animate .bar {
            animation: progress-bar 6s ease both;
        }

        #progress.animate.mobile .bar {
            animation: progress-bar 3s ease both;
        }

        #progress.loading .bar {
            animation: progress-bar 2s ease both;
        }

        #loading-screen {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            background: #fdfcf5;
            animation: fadeOut 1s ease 7s both;
        }

        #loading-screen.mobile {
            animation: fadeOut 1s ease 3s both;
        }

        #loading-screen img {
            height: 90vh;
        }

        .website--content {
            opacity: 0;
            animation: fadeIn 1s ease 7s both;
        }

        .website--content.mobile {
            animation: fadeIn 1s ease 4s both;
        }

        .website--content.mobile .page {
            /*menu height*/
            margin-top: 69px;
        }

        .website--content #app .pages-stack {
            display: unset;
        }

        .website--content #app.loading-page .pages-stack {
            display: none;
        }

        @keyframes progress-bar {
            to {
                width: 100%;
            }
        }

        @keyframes fadeIn {
            to {
                opacity: 1;
            }
        }

        @keyframes fadeOut {
            to {
                opacity: 0;
                z-index: -1;
            }
        }
    </style>

    <!-- Plugins -->
    <link href="http://static.springair.com.pt/frontend/plugins/page-stack-navigation/css/page-stack-navigation.min.css"
          rel="stylesheet" type="text/css" media="all">
    <link href="http://static.springair.com.pt/frontend/plugins/jquery.scrollbar/jquery.scrollbar.min.css"
          rel="stylesheet" type="text/css" media="all">
    <link href="http://static.springair.com.pt/frontend/plugins/slick-1.8.0/slick.min.css" rel="stylesheet"
          type="text/css" media="all">
    <link href="http://static.springair.com.pt/frontend/plugins/slick-1.8.0/slick-theme.min.css" rel="stylesheet"
          type="text/css" media="all">
    <link href="http://static.springair.com.pt/frontend/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css"
          media="all">
    <link href="http://static.springair.com.pt/frontend/plugins/jquery-wizard/jquery-wizard.min.css" rel="stylesheet"
          type="text/css" media="all">
    <link href="http://static.springair.com.pt/frontend/plugins/jquery-asRange/jquery-asRange.min.css" rel="stylesheet"
          type="text/css" media="all">
    <link href="http://static.springair.com.pt/frontend/plugins/animatecss/animate.min.css" rel="stylesheet"
          type="text/css" media="all">
    <link href="http://static.springair.com.pt/frontend/plugins/full-calendar/fullcalendar.min.css" rel="stylesheet"
          type="text/css" media="all">
    <link href="http://static.springair.com.pt/frontend/plugins/full-calendar/fullcalendar.print.min.css"
          rel="stylesheet" type="text/css" media="print">
    <link href="http://static.springair.com.pt/frontend/plugins/material-time-picker/mdtimepicker.min.css"
          rel="stylesheet" type="text/css" media="all">
    <link href="http://static.springair.com.pt/frontend/plugins/select2/css/select2.min.css" rel="stylesheet"
          type="text/css" media="all">

    @yield('css')

    <!-- Styles -->
    <link href="{{ asset('build/css/app.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="http://static.springair.com.pt/js/html5shiv.min.js"></script>
    <script src="http://static.springair.com.pt/js/respond.min.js"></script>
    <![endif]-->

    <!-- Hotjar Tracking Code for http://www.springair.com.pt/ -->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {hjid: 1174508, hjsv: 6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-86420864-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-86420864-1');
    </script>
</head>

<body>

<!-- Loading Bar -->
<div id="progress" class="@if($obj['isMobile']) mobile @endif">
    <div class="bar"></div>
</div>
<!-- Loading Bar -->

<!-- Loading Screen -->
<section id="loading-screen" class="@if($obj['isMobile']) mobile @endif">
    <div id="loadingbar-frame"></div>

    <img src="http://static.springair.com.pt/frontend/images/loading-screen.gif">
</section>
<!-- Loading Screen -->

<section class="website--content @if($obj['isMobile']) mobile @endif">
    @include('layouts.springair.components.header')

    <div id="app" class="{{ $obj['pages'][0]->style == 1 ? 'black' : 'white' }}">
        @yield('content')

        @include('layouts.springair.components.header.nav')
    </div>

    @include('layouts.springair.components.footer')
</section>

<script>
    var $pages = <?=json_encode($obj)?>,
        scripts = [],
        $formRules = {
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 9
                }
            },
            messages: {
                name: {
                    required: 'Por favor, insira o seu nome'
                },
                email: {
                    required: 'Por favor, insira o seu e-mail',
                    email: 'Por favor, insira um e-mail válido'
                },
                phone: {
                    required: 'Por favor, insira o seu telemóvel',
                    number: 'Por favor, insira um número válido',
                    minlength: 'Por favor, insira um número válido'
                }
            }
        };
</script>

<!-- Frameworks -->
<script src="http://static.springair.com.pt/frontend/js/jquery-3.2.1.min.js"></script>
<script>
    function loadScripts() {
        document.write('<script src="http://static.springair.com.pt/frontend/js/popper.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/frameworks/bootstrap-4.0.0-beta.2/js/bootstrap.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/js/velocity.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/js/jquery.visible.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/js/jquery.touchSwipe.min.js"><\/script>');

        <!-- Google Maps -->
        document.write('<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUR5ng39AcdiNFaxkT2sp-oqsT2txjIQI" async defer><\/script>');

        <!-- Plugins -->
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/jquery.scrollbar/jquery.scrollbar.min.js"><\/script>');

        if (window.innerWidth > 576) {
            document.write('<script src="http://static.springair.com.pt/frontend/plugins/page-stack-navigation/js/modernizr-custom.min.js"><\/script>');
            document.write('<script src="http://static.springair.com.pt/frontend/plugins/page-stack-navigation/js/classie.min.js"><\/script>');
            document.write('<script src="http://static.springair.com.pt/frontend/plugins/page-stack-navigation/js/main.js"><\/script>');
        }

        document.write('<script src="http://static.springair.com.pt/frontend/plugins/slick-1.8.0/slick.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/confetti-js/index.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/jquery-wizard/jquery-wizard.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/toastr/toastr.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/jquery-asRange/jquery-asRange.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/full-calendar/lib/moment.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/full-calendar/fullcalendar.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/material-time-picker/mdtimepicker.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/jquery-validation/jquery.validate.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/select2/js/select2.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/proximity-feedback/charming.min.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/proximity-feedback/nearby.js"><\/script>');
        document.write('<script src="http://static.springair.com.pt/frontend/plugins/proximity-feedback/TweenMax.min.js"><\/script>');

        <!-- Custom -->
        var domain = window.location.href.indexOf('dev.springair') > -1 ? 'http://dev.springair/' : 'http://static.springair.com.pt/';

        document.write('<script src="' + domain + 'build/js/pages.min.js"><\/script>');
        document.write('<script src="' + domain + 'build/js/components.min.js"><\/script>');

        $('#progress').addClass('animate');

        $(document).ready(function () {
            var timer = 6000;

            if (window.innerWidth < 576) {
                timer = 3000;
            }

            setTimeout(function () {
                $('#progress').removeClass('animate');
            }, timer);
        });
    }

    if (window.jQuery) {
        loadScripts();
    } else {
        setTimeout(function () {
            loadScripts();
        }, 50);
    }
</script>

@yield('js')

</body>
</html>