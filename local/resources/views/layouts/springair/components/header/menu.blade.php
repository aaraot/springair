<!-- navigation -->
<nav class="pages-nav">
    <div class="menu">
        <div class="row">
            <div class="col-md-12 text-center">
                <img class="logo" src="{{ asset('frontend/images/logo-black.svg') }}" width="200" height=""
                     alt="Springair logo">
            </div>
        </div>

        <div class="row">
            <div class="d-xs-none d-sm-none col-md-6 left">
                <div class="contacts">
                    <span>Entre em contacto connosco:</span>

                    <span title="Ligue-nos">
                        <a class="link contacts--link" href="tel:{{ CNF_PHONE }}">{{ CNF_PHONE }}</a>
                    </span>

                    <address title="Contacte-nos">
                        <a class="link contacts--link" href="mailto:{{ CNF_EMAIL }}">{{ CNF_EMAIL }}</a>
                    </address>
                </div>
            </div>

            <div class="col-md-6 right">
                <div class="menu--primary">
                    <div class="pages-nav__item">
                        <a class="link link--page menu--link active" href="#home">Home</a>
                    </div>
                    <div class="pages-nav__item">
                        <a class="link link--page menu--link" href="#equipamentos">Equipamentos</a>
                    </div>
                    <div class="pages-nav__item">
                        <a class="link link--page menu--link" href="#" data-menu="menu--services">Serviços</a>
                    </div>
                    {{--<div class="pages-nav__item">--}}
                        {{--<a class="link link--page menu--link" href="#sobre-nos">Sobre Nós</a>--}}
                    {{--</div>--}}
                    <div class="pages-nav__item">
                        <a class="link link--page menu--link" href="#contactos">Contactos</a>
                    </div>
                </div>

                <div class="menu--services">
                    <div class="pages-nav__item">
                        <a class="link link--page menu--link revert active" href="#" data-revert="menu--primary"
                           data-menu="menu--services">
                            <img src="{{ asset('frontend/images/ic_arrow.svg') }}" width="" height=""
                                 alt="ic_arrow">
                        </a>
                        <a class="link link--page menu--link revert active" href="#" data-revert="menu--primary"
                           data-menu="menu--services">Serviços</a>
                    </div>
                    @foreach($obj['services'] as $service)
                        <div class="pages-nav__item">
                            <a class="link link--page menu--link" href="#{{ str_slug($service->title) }}">
                                {{ $service->title }}
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-sm-12 d-md-none contacts--mb">
                <div class="contacts">
                    <span>Entre em contacto connosco:</span>

                    <span title="Ligue-nos">
                        <a class="link contacts--link" href="tel:{{ CNF_PHONE }}">{{ CNF_PHONE }}</a>
                    </span>

                    <address title="Contacte-nos">
                        <a class="link contacts--link" href="mailto:{{ CNF_EMAIL }}">{{ CNF_EMAIL }}</a>
                    </address>
                </div>
            </div>
        </div>
    </div>

    <div class="pages-nav__item pages-nav__item--social">
        @foreach($obj['socials'] as $social)
            <a class="link link--social link--faded" href="{{ $social->link }}" target="_blank">
                <i class="fa fa-{{ $social->icon }}"></i>
                <span class="text-hidden">{{ $social->name }}</span>
            </a>
        @endforeach
    </div>
</nav>
<!-- /navigation -->