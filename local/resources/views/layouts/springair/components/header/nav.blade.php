<nav class="menu">
    <div>
        <button class="menu-button"><span>Menu</span>
        </button>

        <span class="menu-logo">
            @include('pages.components.logo')
        </span>

        <button class="search-button wrapper search">
            <div class="element" id="one"></div>
            <div class="element" id="two"></div>
        </button>

        <button class="close-button"
                onclick="$homepage.closeWizard()">
            <div class="element" id="one"></div>
            <div class="element" id="two"></div>
        </button>
    </div>
</nav>