@foreach($results['equipments'] as $result)
    <li class="btn--goTo" data-link="equipamentos#{{ str_slug($result->name) }}">
        <div class="row">
            <div class="col-2"
                 style="background: url(http://static.springair.com.pt/uploads/equipments/{{ $result->image }}) no-repeat center"></div>

            <div class="col-9 text-left" style="margin-left: 32px">
                <span class="search-nav--result-item--title">{{ $result->name }}</span>

                <p class="search-nav--result-item--text">{!! $result->description !!}</p>
            </div>
        </div>
    </li>
@endforeach

@foreach($results['services'] as $result)
    <li class="btn--goTo" data-link="{{ str_slug($result->title) }}">
        <div class="row">
            <div class="col-2"
                 style="background: url(http://static.springair.com.pt/uploads/services/{{ $result->image }}) no-repeat center"></div>

            <div class="col-9 text-left" style="margin-left: 32px">
                <span class="search-nav--result-item--title">{{ $result->title }}</span>

                <p class="search-nav--result-item--text">{!! $result->description !!}</p>
            </div>
        </div>
    </li>
@endforeach