@foreach($results['equipments'] as $result)
    <li>
        <a href="/equipamentos#{{ str_slug($result->name) }}">
            <div class="row">
                <div class="col-2"
                     style="background: url(http://static.springair.com.pt/uploads/equipments/{{ $result->image }}) no-repeat center"></div>

                <div class="col-10">
                    <span class="search-nav--result-item--title">{{ $result->name }}</span>
                </div>

                <div class="col-12 p-0">
                    <p class="search-nav--result-item--text">{!! $result->description !!}</p>
                </div>
            </div>
        </a>
    </li>
@endforeach

@foreach($results['services'] as $result)
    <li>
        <a href="/{{ str_slug($result->title) }}">
            <div class="row">
                <div class="col-2"
                     style="background: url(http://static.springair.com.pt/uploads/services/{{ $result->image }}) no-repeat center"></div>

                <div class="col-10">
                    <span class="search-nav--result-item--title">{{ $result->title }}</span>
                </div>

                <div class="col-12 p-0">
                    <p class="search-nav--result-item--text">{!! $result->description !!}</p>
                </div>
            </div>
        </a>
    </li>
@endforeach