<!-- search -->
<nav class="search-nav">
    <div class="search">
        <div class="row">
            <div class="col-md-12">
                <img class="logo" src="{{ asset('frontend/images/logo-black.svg') }}" width="200" height=""
                     alt="Springair logo">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="search-container">
                    <input type="text" name="search" placeholder="Pesquise aqui...">
                    <span class="bar"></span>

                    <span class="search-container--info">Utilize o ENTER para pesquisar ou ESC para sair.</span>
                </div>
            </div>
        </div>

        <div class="search-tips-container">
            <div class="row">
                <div class="col-md-6 search-tips--col">
                    <span class="title search--title">Podemos sugerir uma pesquisa?</span>

                    <ul class="search-tips-list">
                        @foreach($obj['searchSuggestions'] as $suggestion)
                            <li class="search-tips-list--item">
                                <a href="javascript:void(0)" class="btn--goTo"
                                   data-link="@if($suggestion->page_id){{ $suggestion->page_url }}@else{{ $suggestion->equipment_url }}@endif">{{ $suggestion->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="col-md-6 search-tips--col">
                    <span class="title search--title">Mais pesquisados:</span>

                    <ul class="search-tips-list">
                        @foreach($obj['topSearches'] as $search)
                            <li class="search-tips-list--item">
                                <a href="javascript:void(0)" class="btn--goTo"
                                   data-link="@if($search->page_id){{ $search->page_url }}@else{{ $search->equipment_url }}@endif">{{ $search->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <div class="search-nav--result-list--container d-none">
            <div class="row">
                <div class="col-12">
                    <span class="title search--title d-block">Resultados da pesquisa</span>
                    <span class="title search--results d-block">
                        Encontrámos
                        <span class="search--results--counter"></span>
                        resultados de peqsuisa
                    </span>

                    <div class="scrollbar-macosx">
                        <ul class="search-nav--result-list"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>

<nav class="search-nav--mb hidden">
    <div class="search-nav--search-container">
        <input type="text" name="search" placeholder="Pesquise aqui...">

        <a class="search-button--close" onclick="$searchButton.closeMB()">
            <img src="{{ asset('frontend/images/ic_close.svg') }}" width="" height="" alt="ic_close">
        </a>
    </div>

    <div class="search-nav--result-list--container">
        <span class="title search--title">Resultados da pesquisa</span>
        <span class="title search--results d-block">
            Encontrámos
            <span class="search--results--counter"></span>
            resultados de peqsuisa
        </span>

        <ul class="search-nav--result-list"></ul>
    </div>
</nav>
<!-- /search -->