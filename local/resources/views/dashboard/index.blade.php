@extends('layouts.app')

@section('content')
    <script type="text/javascript" src="{{ asset('frontend/js/chart.min.js') }}"></script>

    <div class="page-content row">
        <div class="page-content-wrapper m-t">
            @if(Auth::check() && Auth::user()->group_id == 1)
                <section class="ribon-sximo">
                    <div class="row m-l-none m-r-none m-t white-bg shortcut ribon ">
                        <div class="col-sm-6 col-md-3 p-sm ribon-white">
                            <span class="pull-left m-r-sm"><i class="fa fa-table"></i></span>
                            <a href="{{ URL::to('sximo/module') }}" class="clear">
				                <span class="h3 block m-t-xs">
                                    <strong>{{ Lang::get('core.dash_i_module') }}</strong>
				                </span>

                                <small>{{ Lang::get('core.dash_module') }}</small>
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 p-sm ribon-module">
                            <span class="pull-left m-r-sm">
                                <i class="icon-steam2"></i>
                            </span>

                            <a href="{{ URL::to('sximo/config') }}" class="clear">
				                <span class="h3 block m-t-xs">
                                    <strong>{{ Lang::get('core.dash_i_setting') }}</strong>
				                </span>

                                <small>{{ Lang::get('core.dash_setting') }}</small>
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 p-sm ribon-white">
                            <span class="pull-left m-r-sm">
                                <i class="icon-list"></i>
                            </span>

                            <a href="{{ URL::to('sximo/menu') }}" class="clear">
                                <span class="h3 block m-t-xs">
                                    <strong>{{ Lang::get('core.dash_i_sitemenu') }}</strong>
                                </span>

                                <small>{{ Lang::get('core.dash_sitemenu') }}</small>
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 p-sm ribon-setting">
                            <span class="pull-left m-r-sm">
                                <i class="icon-users"></i>
                            </span>

                            <a href="{{ URL::to('core/users') }}" class="clear">
                                <span class="h3 block m-t-xs">
                                    <strong>{{ Lang::get('core.dash_i_usergroup') }}</strong>
			                    </span>

                                <small>{{ Lang::get('core.dash_usergroup') }}</small>
                            </a>
                        </div>
                    </div>
                </section>
            @endif

            <div class="row m-t">
                <div class="col-lg-12">
                </div>
            </div>
        </div>
    </div>
@stop