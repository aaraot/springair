// Gulp.js configuration
// modules
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    concat = require('gulp-concat'),
    deporder = require('gulp-deporder'),
    stripdebug = require('gulp-strip-debug'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    minifyCss = require("gulp-minify-css"),
    folder = {
        src: '../frontend/',
        build: '../build/'
    }; // folders

// JavaScript pages processing
gulp.task('pages', function () {
    return gulp.src(folder.src + 'js/pages/**/*')
        .pipe(deporder())
        .pipe(concat('pages.min.js'))
        // .pipe(stripdebug())
        .pipe(uglify().on('error', function (e) {
            console.log(e);
        }))
        .pipe(gulp.dest(folder.build + 'js/'));
});

// JavaScript pages processing
gulp.task('pages-mobile', function () {
    return gulp.src(folder.src + 'js/pages/**/*')
        .pipe(deporder())
        // .pipe(concat('pages.min.js'))
        // .pipe(stripdebug())
        .pipe(uglify().on('error', function (e) {
            console.log(e);
        }))
        .pipe(gulp.dest(folder.build + 'js/'));
});

// JavaScript components processing
gulp.task('components', function () {
    return gulp.src(folder.src + 'js/components/**/*')
        .pipe(deporder())
        .pipe(concat('components.min.js'))
        // .pipe(stripdebug())
        .pipe(uglify().on('error', function (e) {
            console.log(e);
        }))
        .pipe(gulp.dest(folder.build + 'js/'));
});

// Sass components processing
gulp.task('styles', function(){
    return gulp.src(folder.src + 'css/app.scss')
        .pipe(sass())
        .pipe(minifyCss())
        .pipe(gulp.dest(folder.build + 'css/'))
});

gulp.task('watch', function () {
    // Watch for pages changes
    gulp.watch(folder.src + 'js/pages/**/*', ['pages']);

    // Watch for pages changes
    gulp.watch(folder.src + 'js/pages/**/*', ['pages-mobile']);

    // Watch for components changes
    gulp.watch(folder.src + 'js/components/**/*', ['components']);

    // Watch for styles changes
    gulp.watch(folder.src + 'css/app.css', ['styles']);
});

// Run all tasks
gulp.task('run', ['pages', 'pages-mobile', 'components', 'styles']);