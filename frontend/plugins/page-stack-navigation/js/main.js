/**
 * main.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2015, Codrops
 * http://www.codrops.com
 */
;(function (window) {

    'use strict';

    var support = {transitions: Modernizr.csstransitions},
        // transition end event name
        transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd',
            'MozTransition': 'transitionend',
            'OTransition': 'oTransitionEnd',
            'msTransition': 'MSTransitionEnd',
            'transition': 'transitionend'
        },
        transEndEventName = transEndEventNames[Modernizr.prefixed('transition')],
        onEndTransition = function (el, callback) {
            var onEndCallbackFn = function (ev) {
                if (support.transitions) {
                    if (ev.target != this) return;
                    this.removeEventListener(transEndEventName, onEndCallbackFn);
                }
                if (callback && typeof callback === 'function') {
                    callback.call(this);
                }
            };
            if (support.transitions) {
                el.addEventListener(transEndEventName, onEndCallbackFn);
            } else {
                onEndCallbackFn();
            }
        },
        // the pages wrapper
        stack = document.querySelector('.pages-stack'),
        // the page elements
        pages = [].slice.call(stack.children),
        // total number of page elements
        pagesTotal = pages.length,
        // index of current page
        current = 0,
        // menu button
        menuCtrl = document.querySelector('button.menu-button'),
        // search button
        searchCtrl = document.querySelector('button.search-button'),
        // the navigation wrapper
        nav = document.querySelector('.pages-nav'),
        // the search wrapper
        search = document.querySelector('.search-nav'),
        // the menu nav items
        navItems = [].slice.call(nav.querySelectorAll('.link--page')),
        // check if menu is open
        isMenuOpen = false,
        isSearchOpen = false,
        // services
        services = [],
        // scroll target
        target = $('.pages-stack .page .scroll-content').length > 0 ? $('.page:not(.page--inactive) .scrollbar-macosx > div') : $('.page:not(.page--inactive) > div');

    function init() {
        buildStack();
        initEvents();
        initPage(window.location);

        $('.menu .menu-logo').click(function () {
            initPage('home');
        });
    }

    function buildStack() {
        var stackPagesIdxs = getStackPagesIdxs();

        // set z-index, opacity, initial transforms to pages and add class page--inactive to all except the current one
        for (var i = 0; i < pagesTotal; ++i) {
            var page = pages[i],
                posIdx = stackPagesIdxs.indexOf(i);

            if (current !== i) {
                classie.add(page, 'page--inactive');

                if (posIdx !== -1) {
                    // visible pages in the stack
                    page.style.WebkitTransform = 'translate3d(0,120%,0)';
                    page.style.transform = 'translate3d(0,120%,0)';
                } else {
                    // invisible pages in the stack
                    page.style.WebkitTransform = 'translate3d(0,120%,-300px)';
                    page.style.transform = 'translate3d(0,120%,-300px)';
                }
            } else {
                classie.remove(page, 'page--inactive');
            }

            page.style.zIndex = i < current ? parseInt(current - i) : parseInt(pagesTotal + current - i);

            if (posIdx !== -1) {
                page.style.opacity = parseFloat(1 - 0.1 * posIdx);
            } else {
                page.style.opacity = 0;
            }
        }
    }

    // event binding
    function initEvents() {
        // menu button click
        menuCtrl.addEventListener('click', toggleMenu);
        // search button click
        searchCtrl.addEventListener('click', toggleSearch);
        // card link click
        $('.card--link').click(function () {
            goTo($(this));

            // Set active menu
            $('.menu--primary').attr('style', 'transform: translateX(-100%);');
            $('.menu--services').attr('style', 'transform: translateX(0);');
            // Set active menu item
            $('.link--page').removeClass('active');
            $('.link--page[href="#' + $(this).attr('data-link') + '"]').addClass('active');
        });
        // button link click
        $(document).on('click', '.btn--goTo', function () {
            goTo($(this));

            // Reset menu
            $('.search-container input').val('');
            $('.menu--primary').removeAttr('style');
            $('.menu--services').removeAttr('style');
        });

        // navigation menu clicks
        navItems.forEach(function (item) {
            if (!item.getAttribute('data-menu')) {
                // which page to open?
                var pageid = item.getAttribute('href').slice(1);
                item.addEventListener('click', function (ev) {
                    ev.preventDefault();
                    setMenuItem(pageid);
                    openPage(pageid);
                });
            } else {
                // which menu to open or close?
                if (item.getAttribute('data-revert')) {
                    item.addEventListener('click', function (ev) {
                        ev.preventDefault();

                        $('.' + item.getAttribute('data-revert')).removeAttr('style');
                        $('.' + item.getAttribute('data-menu')).removeAttr('style');
                    });
                } else {
                    item.addEventListener('click', function (ev) {
                        ev.preventDefault();

                        $('.menu--primary').attr('style', 'transform: translateX(-100%);');
                        $('.' + item.getAttribute('data-menu')).attr('style', 'transform: translateX(0);');
                    });
                }
            }
        });

        // clicking on a page when the menu is open triggers the menu to close again and open the clicked page
        pages.forEach(function (page) {
            var pageid = page.getAttribute('id');
            page.addEventListener('click', function (ev) {
                if (isMenuOpen) {
                    ev.preventDefault();
                    openPage(pageid);
                } else if (isSearchOpen) {
                    ev.preventDefault();
                    openPage2(pageid);
                }
            });
        });

        // keyboard navigation events
        document.addEventListener('keydown', function (ev) {
            if (!isMenuOpen) return;
            if (!isSearchOpen) return;
            var keyCode = ev.keyCode || ev.which;
            if (keyCode === 27) {
                closeMenu();

                if ($('.search-button').hasClass('_close')) {
                    $('.search-button').click();
                }
            }
        });

        // get all service pages
        $('.page.services').each(function () {
            services.push($(this).attr('id'));
        })
    }

    // toggle menu fn
    function toggleMenu() {
        if (isMenuOpen) {
            closeMenu();
        } else {
            openMenu();
            isMenuOpen = true;
        }
    }

    // Set active menu item
    function setMenuItem(id) {
        id = id === '' ? 'home' : id;

        if (services.includes(id)) {
            // Show services menu
            $('.menu--primary').attr('style', 'transform: translateX(-100%);');
            $('.menu--services').attr('style', 'transform: translateX(0);');
        }

        $('.link--page').removeClass('active');
        $('.link--page[href="#' + id + '"]').addClass('active');
    }

    // toggle menu fn
    function toggleSearch() {
        if (isSearchOpen) {
            closeSearch();
        } else {
            if ($(window).width() > 576) {
                openSearch();
                isSearchOpen = true;
            } else {
                // toggle the search mb
                $('.search-nav--mb').removeClass('hidden');
                $('nav[class^="search-nav"] input').focus();
            }
        }
    }

    // opens the menu
    function openMenu() {
        // toggle the menu button
        classie.add(menuCtrl, 'menu-button--open');
        // hide the search button
        classie.add(searchCtrl, 'hidden');
        // stack gets the class "pages-stack--open" to add the transitions
        classie.add(stack, 'pages-stack--open');
        // reveal the menu
        classie.add(nav, 'pages-nav--open');
        // hide the menu logo
        $('.menu-logo').attr('style', 'visibility: hidden;');
        // remove class down from menu
        $('#app .menu').removeClass('down');

        // now set the page transforms
        var stackPagesIdxs = getStackPagesIdxs(), y = 118;
        for (var i = 0, len = stackPagesIdxs.length; i < len; ++i) {
            var page = pages[stackPagesIdxs[i]];
            y += 2;

            if ($(window).height() <= 667) {
                y = 150;
            }

            page.style.WebkitTransform = 'translate3d(0, ' + y + '%, ' + parseInt(-1 * 200 - 50 * i) + 'px)'; // -200px, -230px, -260px
            page.style.transform = 'translate3d(0, ' + y + '%, ' + parseInt(-1 * 200 - 50 * i) + 'px)';
        }
    }

    // opens the search
    function openSearch() {
        // hide the menu button
        classie.add(menuCtrl, 'hidden');
        // stack gets the class "search-stack--open" to add the transitions
        classie.add(stack, 'pages-stack--open');
        // reveal the search
        classie.add(search, 'pages-nav--open');
        // remove class down from menu
        $('#app .menu').removeClass('down');

        // now set the page transforms
        var stackPagesIdxs = getStackPagesIdxs(), y = 118;
        for (var i = 0, len = stackPagesIdxs.length; i < len; ++i) {
            var page = pages[stackPagesIdxs[i]];
            y += 2;

            if ($(window).height() <= 780) {
                y = 150;
            }

            page.style.WebkitTransform = 'translate3d(0, ' + y + '%, ' + parseInt(-1 * 200 - 50 * i) + 'px)'; // -200px, -230px, -260px
            page.style.transform = 'translate3d(0, ' + y + '%, ' + parseInt(-1 * 200 - 50 * i) + 'px)';
        }
    }

    // closes the menu
    function closeMenu() {
        // same as opening the current page again
        openPage();
        // reset navbar
        setTimeout(function () {
            $nav.reset();
            $()
        }, 500);
    }

    // closes the search
    function closeSearch() {
        // same as opening the current page again
        openPage2();
        // reset navbar
        setTimeout(function () {
            $nav.reset();
            $('.search-container input').val('');
        }, 500);
    }

    // Go to correct stacked page
    function initPage(id) {
        var _id = jQuery.type(id) !== "string" ? window.location.pathname.replace('/', '') : id.split('#')[0];
        var futurePage = _id ? document.getElementById(_id) : pages[current],
            futureCurrent = pages.indexOf(futurePage),
            stackPagesIdxs = getStackPagesIdxs(futureCurrent);

        // set transforms for the new current page
        futurePage.style.WebkitTransform = 'translate3d(0, 0, 0)';
        futurePage.style.transform = 'translate3d(0, 0, 0)';
        futurePage.style.opacity = 1;

        // set transforms for the other items in the stack
        for (var i = 0, len = stackPagesIdxs.length; i < len; ++i) {
            var page = pages[stackPagesIdxs[i]];
            page.style.WebkitTransform = 'translate3d(0,120%,0)';
            page.style.transform = 'translate3d(0,120%,0)';
        }

        // Reset scrollbar
        target.animate({
            scrollTop: 0
        }, 0);

        if (_id) {
            setTimeout(function () {
                setMenuItem(_id);
                updateURL(id, 'seo');
            }, 350);
        }

        // Set the menu styles
        $pages.pages.forEach(function (page) {
            var $id = (_id === 'home' || _id === '') ? 'homepage' : _id;

            if (strSlug(page.name) === $id) {
                if (page.style == 0) {
                    $('.menu-button').addClass('white');
                    $('.menu-logo svg #Group').attr('style', 'fill: #fff;');
                    $('.search-button').addClass('white');
                } else {
                    $('.menu-button').removeClass('white');
                    $('.menu-logo svg #Group').attr('style', 'fill: #000;');
                    $('.search-button').removeClass('white');
                }
            }
        });

        setCurrentPage(_id, futureCurrent);
        buildStack();
        removeTransforms(futurePage);

        return {futurePage: futurePage, futureCurrent: futureCurrent, stackPagesIdxs: stackPagesIdxs};
    }

    // On card link click
    // Go to respective page
    function goTo(elem) {
        var id = elem.attr('data-link');

        $('.menu').removeClass('down');

        if (isMenuOpen) {
            openPage(id);
        } else if (isSearchOpen) {
            openPage2(id);
        } else {
            initPage(id);
        }
    }

    // opens a page
    function openPage(id) {
        var obj = initPage(id);

        // close menu..
        classie.remove(menuCtrl, 'menu-button--open');
        classie.remove(searchCtrl, 'hidden');
        classie.remove(nav, 'pages-nav--open');
        $('.menu-logo').removeAttr('style');
        onEndTransition(obj.futurePage, function () {
            classie.remove(stack, 'pages-stack--open');
            // reorganize stack
            buildStack();
            isMenuOpen = false;
            isSearchOpen = false;
        });

        removeTransforms(obj.futurePage);

        if (id) {
            updateURL(id, 'seo');
        }
    }

    // opens a page
    // when search is visible
    function openPage2(id) {
        var obj = initPage(id);

        // close search..
        classie.remove(menuCtrl, 'hidden');
        classie.remove(search, 'pages-nav--open');
        $searchButton.close();
        onEndTransition(obj.futurePage, function () {
            classie.remove(stack, 'pages-stack--open');
            // reorganize stack
            buildStack();
            isMenuOpen = false;
            isSearchOpen = false;
        });

        removeTransforms(obj.futurePage);
    }

    // Set the current page
    function setCurrentPage(id, futureCurrent) {
        if (id) {
            current = futureCurrent;
        }
    }

    // Removes transforms from future page
    function removeTransforms(futurePage) {
        setTimeout(function () {
            futurePage.style.removeProperty('WebkitTransform');
            futurePage.style.removeProperty('transform');
        }, 500);
    }

    // Update the address bar
    function updateURL(id, requestType) {
        var equipment = null;
        if (jQuery.type(id) !== "string") {
            id = window.location.pathname.replace('/', '');
            equipment = window.location.hash;
        } else {
            var idSplit = id.split('#');
            id = idSplit[0];
            equipment = idSplit.length > 1 ? '#' + idSplit[1] : '';
        }
        var url = id === 'home' ? '/' : '/' + id + equipment;

        $.get(url, {type: requestType}, function (data) {
            setTimeout(function () {
                $('title').text(data.title);
                $('meta[itemprop="name"], meta[itemprop="og:title"], meta[name="twitter:title"]').attr('content', data.title);
                $('meta[name="description"], meta[itemprop="description"], meta[name="og:description"], meta[name="twitter:description"]').attr('content', data.description);
                $('meta[name="keywords"]').attr('content', data.keywords);
                $('link[rel="canonical"]').attr('href', window.location.protocol + '//' + window.location.host + url);
            }, 500);
        });

        window.history.pushState(null, null, url);

        if (equipment !== '') {
            target.animate({
                scrollTop: $('.page:not(.page--inactive) ' + equipment).position().top
            }, 800);

            setTimeout(function () {
                $('.page:not(.page--inactive) ' + equipment + ' .btn--container .btn').click();
            }, 800);
        }
    }

    function strSlug(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "ãàáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return str;
    }

    // gets the current stack pages indexes. If any of them is the excludePage then this one is not part of the returned array
    function getStackPagesIdxs(excludePageIdx) {
        var nextStackPageIdx = current + 1 < pagesTotal ? current + 1 : 0,
            nextStackPageIdx_2 = current + 2 < pagesTotal ? current + 2 : 1,
            idxs = [],

            excludeIdx = excludePageIdx || -1;

        if (excludePageIdx != current) {
            idxs.push(current);
        }
        if (excludePageIdx != nextStackPageIdx) {
            idxs.push(nextStackPageIdx);
        }
        if (excludePageIdx != nextStackPageIdx_2) {
            idxs.push(nextStackPageIdx_2);
        }

        return idxs;
    }

    init();

})(window);