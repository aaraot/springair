var $services = null;

(function () {
    'use strict';

    $services = {
        initialize: function () {
            $(document).on('click', '.page.services:not(.page--inactive) a[href^="#"]:not(.nav-link)', function (event) {
                event.preventDefault();

                setTimeout(function () {
                    $('.page.services:not(.page--inactive) .card').removeClass('opacity-0').addClass('animated fadeInUp');
                }, 350);

                $('.page.services:not(.page--inactive) .scroll-content').animate({
                    scrollTop: $($.attr(this, 'href')).offset().top - 100
                }, 800);
            });

            var target = $('.pages-stack .page .scroll-content').length > 0 ? $('.services .scrollbar-macosx > div') : $('.services > div');

            target.on('scroll', function () {
                var st = $(this).scrollTop();

                if (st !== 0) {
                    // downscroll code
                    $('.page.services:not(.page--inactive) .card').removeClass('opacity-0').addClass('animated fadeInUp');
                }
            });

            if ($(window).width() <= 1024) {
                target.swipe({
                    //Generic swipe handler for all directions
                    swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
                        if ($('.show-wizard').length === 0) {
                            if (direction === 'up') {
                                // downscroll code
                                $('.page.services:not(.page--inactive) .card').removeClass('opacity-0').addClass('animated fadeInUp');
                            }
                        }
                    },
                    allowPageScroll: "vertical"
                });
            }

            if ($(window).width() <= 576) {
                $('.service-details').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true
                });

                $('.covers-list').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true
                });
            }
        }
    }

})();