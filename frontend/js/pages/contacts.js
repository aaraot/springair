var $contacts = null, contactForm = null, map = null;

(function () {
    'use strict';

    $contacts = {
        initialize: function () {
            $contacts.initMap();

            contactForm = $('.contacts form').validate($formRules);

            $('.contacts form').on('submit', function (e) {
                e.preventDefault();

                if (contactForm.valid()) {
                    var data = {
                        name: $('.contacts #name').val(),
                        email: $('.contacts #email').val(),
                        phone: $('.contacts #phone').val(),
                        message: $('.contacts #message').val()
                    };

                    $.post('/contact', {data: data}, function (data) {
                        if (data.submitted) {
                            var image = $('.contacts img').eq(0),
                                form = $('.contacts form');

                            image.addClass('opacity-0');
                            form.addClass('opacity-0');

                            setTimeout(function () {
                                image.toggle();
                                form.toggle();
                                $('.contacts .form-submitted').height($('.contacts .form-submitted').height()).toggle().addClass('opacity-1');
                            }, 350);
                        } else {
                            toastr.error('Ocorreu um erro. Por favor, tente submeter o formulário outra vez.', 'Erro!');
                        }
                    });
                }
            });

            $('.contacts .btn--goTo').click(function () {
                $contacts.reset();
                var image = $('.contacts img').eq(0),
                    form = $('.contacts form');

                image.toggle().removeClass('opacity-0');
                form.toggle().removeClass('opacity-0');
                $('.contacts .form-submitted').toggle().addClass('opacity-0');
            });
        },

        initMap: function () {
            // The location of Springair
            var springair = {lat: 41.1834429, lng: -8.6879762};
            // The map, centered at Springair
            if (typeof google === 'object' && typeof google.maps === 'object' && $('#map').length > 0) {
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 18,
                    center: springair,
                    mapTypeId: 'roadmap',
                    styles: [
                        {
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f5f5f5"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#616161"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#f5f5f5"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.land_parcel",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#bdbdbd"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#eeeeee"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#e5e5e5"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#9e9e9e"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#dadada"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#616161"
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#9e9e9e"
                                }
                            ]
                        },
                        {
                            "featureType": "transit.line",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#e5e5e5"
                                }
                            ]
                        },
                        {
                            "featureType": "transit.station",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#eeeeee"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#c9c9c9"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#9e9e9e"
                                }
                            ]
                        }
                    ]
                });
                // Icons
                var iconBase = 'http://static.springair.com.pt/frontend/images/';
                var icon = iconBase + 'location.png';
                // The marker, positioned at Springair
                var marker = new google.maps.Marker({
                    position: springair,
                    icon: {
                        url: icon, // url
                        scaledSize: new google.maps.Size(100, 100) // scaled size
                    },
                    map: map
                });
            }
        },

        reset: function () {
            $('.contacts input, .contacts textarea').each(function () {
                $(this).parents('.mat-div').removeClass('is-completed');
                $(this).val('');
            });
        }
    }

})();