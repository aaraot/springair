var $homepage = null, init = 0;

(function () {
    'use strict';

    $homepage = {
        initialize: function () {
            if ($('#app').hasClass('white')) {
                $('.menu-logo svg #Group').attr('style', 'fill: #fff;');
            }

            if (navigator.userAgent.indexOf('Mac OS') === -1) {
                $('.scrollbar-macosx').scrollbar({ignoreMobile: true, ignoreOverlay: true});
            }

            $('.clients-list').slick({
                arrows: false,
                slidesToShow: 8,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 6,
                            slidesToScroll: 3,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 2,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 576,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 2,
                            infinite: true
                        }
                    }
                ]
            });

            if ($(window).width() <= 576) {
                $('.services-list').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true
                });
            }

            $('.asRange').asRange({tip: false});
            $('.asRange').on('asRange::change', function (e) {
                var text = e.target.value + ' m2';

                if (e.target.value == 45) {
                    text = 'menos de 45m2';
                } else if (e.target.value == 495) {
                    text = 'mais de 500m2';
                }
                $('.wizard-range-slider--value').text(text);
            });

            $('.feature-list--item').click(function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                } else {
                    $(this).addClass('selected');
                }
            });
        },

        showWizard: function (elem, wizard) {
            $('.page:not(.page--inactive)').addClass('no-scroll');
            $('#app .menu').removeClass('down');
            $('.menu-logo svg #Group').attr('style', 'fill: #000;');
            elem.parents(wizard).next().addClass('show-wizard');

            setTimeout(function () {
                $('.menu-button, .search-button, #cd-vertical-nav').removeClass('opacity-1').addClass('opacity-0');
                $('.show-wizard').attr('style', 'transform: translateX(0);');
            }, 0);

            setTimeout(function () {
                $('.close-button').removeClass('opacity-0').addClass('opacity-1');

                if (init === 0) {
                    $customWizard.initialize();
                    init++;
                }

                if ($('.show-wizard').hasClass('wizard--services')) {
                    $meetingWizard.initialize();
                }
            }, 400);
        },

        closeWizard: function () {
            $('.scrollbar-macosx').scrollbar();
            $('.page:not(.page--inactive)').removeClass('no-scroll');
            $('#app .menu').addClass('down');
            $('.menu-logo svg #Group').removeAttr('style');
            $('.show-wizard').attr('style', 'transform: translateX(100%);');

            setTimeout(function () {
                $('.close-button').removeClass('opacity-1').addClass('opacity-0');
            }, 0);

            setTimeout(function () {
                $('.menu-button, .search-button, #cd-vertical-nav').removeClass('opacity-0').addClass('opacity-1');
                $('.show-wizard').removeClass('show-wizard');
            }, 1000);

            setTimeout(function () {
                wizard.wizard('reset');
                $customWizard.reset();
                $meetingWizard.reset();
                $fragranceWizard.reset();
            }, 1050);
        }
    }

})();