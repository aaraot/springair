var $equipments = null, sections = [], budgetForm = null;

(function () {
    'use strict';

    $equipments = {
        initialize: function () {
            $('.equipment-categories').select2({
                placeholder: 'Procuro equipamento para...',
                allowClear: true
            });

            $('.equipment-categories').on('select2:select', function (e) {
                $.post('/filter-equipments', {id: $(this).val()}, function (data) {
                    $('#equipments').html(data);
                });
            });

            $('.equipment-categories').on('select2:unselecting', function (e) {
                var elem = $(this);
                setTimeout(function () {
                    $.post('/filter-equipments', {id: elem.val()}, function (data) {
                        $('#equipments').html(data);
                    });
                }, 0);
            });

            $('.dropdown-item').click(function () {
                $('.search--container .dropdown-toggle').text($(this).text());

                $.post('/filter-equipments', {id: $(this).attr('data-filter')}, function (data) {
                    $('#equipments').html(data);
                });
            });

            $(document).on('click', '.see-demo', function () {
                $('#progress').addClass('loading');

                $.post('/equipments-detail', {
                    id: $(this).attr('data-id'),
                    mobile: $(window).width() <= 567 ? true : false
                }, function (data) {
                    setTimeout(function () {
                        $('.equipment--detail--container').html(data);

                        setTimeout(function () {
                            $equipments.show();
                            $input.initialize();
                            $('#progress').removeClass('loading');

                            if ($(window).width() <= 576) {
                                $('.covers-list').slick({
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    arrows: false,
                                    dots: true
                                });
                            } else {
                                $mouseWheel.initialize();
                            }
                        }, 150);
                    }, 2000);
                });
            });

            // Details events
            $(document).on('click', '.link.close', function () {
                $equipments.return();
            });

            $(document).on('click', '.link.left', function () {
                $equipments.swipe('right');
            });

            $(document).on('click', '.link.right', function () {
                $equipments.swipe('left');
            });

            $(document).on('click', '.page[id^="equipamentos"] a[href^="#"]:not(.nav-link)', function (event) {
                event.preventDefault();

                $('.equipment--detail > div').animate({
                    scrollTop: $($.attr(this, 'href')).position().top
                }, 800);
            });
        },

        show: function () {
            if ($(window).width() <= 567) {
                $equipments.hideMenu('.equipment--detail--mb');
            } else {
                var width = $('.equipment--detail').width() - (2 * $('.equipment--detail--image').outerWidth());

                if ($(window).width() <= 1200) {
                    width = '100%';
                    $('.equipment--detail--content').addClass('col-9');
                }

                if ($(window).width() <= 992) {
                    $('.w-50').addClass('w-75');
                }

                $('.equipment--detail--content').attr('style', 'left: ' + $('.equipment--detail--image').outerWidth() + 'px;max-width: ' + width + 'px;width: ' + width + 'px;flex: ' + width + 'px;');
                $('.horizontal-menu').attr('style', 'left:' + $('.equipment--detail--image').outerWidth() + 'px;width:' + $('.equipment--detail--content').outerWidth() + 'px;');

                $equipments.hideMenu('.equipment--detail');
            }

            $('.equipment--detail > div').on('scroll', function () {
                $equipments.scroll(sections);
            });

            $('.equipment--detail section').each(function () {
                sections.push($(this).attr('id'));
            });


            if ($(window).width() <= 1024) {
                $('.equipment--detail--content section').swipe({
                    //Generic swipe handler for all directions
                    swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
                        $equipments.swipe(direction);
                    },
                    allowPageScroll: "vertical"
                });
            }

            budgetForm = $('#eq-budget form').validate($formRules);

            $('#eq-budget form').on('submit', function (e) {
                e.preventDefault();

                if (budgetForm.valid()) {
                    var data = {
                        name: $('#eq-budget #name').val(),
                        email: $('#eq-budget #email').val(),
                        phone: $('#eq-budget #phone').val(),
                        message: $('#eq-budget #message').val()
                    };

                    $.post('/budget', {data: data}, function (data) {
                        if (data.submitted) {
                            var image = $('#eq-budget .img-fluid').eq(0),
                                form = $('#eq-budget form');

                            image.addClass('opacity-0');
                            form.addClass('opacity-0');

                            setTimeout(function () {
                                image.toggle();
                                form.toggle();
                                $('.form-submitted').height($('#eq-budget form').height()).toggle().addClass('opacity-1');
                            }, 350);
                        } else {
                            toastr.error('Ocorreu um erro. Por favor, tente submeter o formulário outra vez.', 'Erro!');
                        }
                    });
                }
            });
        },

        swipe: function (direction) {
            var current = $('.equipment--detail--content section.active');

            if (direction === 'left') {
                if (!current.is(':last-child')) {
                    current.attr('style', 'transform: translateX(-110%)');
                    current.removeClass('active');
                    current.next().addClass('active');

                    $('.equipment--detail--mb .equipment--detail--menu .link.return').first().removeClass('hidden').find('span').text(current.find('.equipment--title').text().split(' (')[0]);
                    $('.equipment--detail--mb .equipment--detail--menu .link.return').last().find('span').text(current.next().next().find('.equipment--title').text().split(' (')[0]);

                    if ($('.equipment--detail--mb .equipment--detail--menu .link.return').last().find('span').text() === '') {
                        $('.equipment--detail--mb .equipment--detail--menu .link.return').last().addClass('hidden');
                    }
                }
            } else if (direction === 'right') {
                if (!current.is(':first-child')) {
                    current.removeClass('active');
                    current.prev().removeAttr('style').addClass('active');

                    $('.equipment--detail--mb .equipment--detail--menu .link.return').first().find('span').text(current.prev().prev().find('.equipment--title').text().split(' (')[0]);
                    $('.equipment--detail--mb .equipment--detail--menu .link.return').last().find('span').text(current.find('.equipment--title').text().split(' (')[0]);

                    if ($('.equipment--detail--mb .equipment--detail--menu .link.return').first().find('span').text() === '') {
                        $('.equipment--detail--mb .equipment--detail--menu .link.return').first().addClass('hidden');
                    }

                    if ($('.equipment--detail--mb .equipment--detail--menu .link.return').last().find('span').text() !== '') {
                        $('.equipment--detail--mb .equipment--detail--menu .link.return').last().removeClass('hidden');
                    }
                }
            }
        },

        hideMenu: function (section) {
            $('#app .menu').removeClass('opacity-1').addClass('opacity-0');
            $('.equipments').addClass('no-scroll');
            $('.menu-button, .search-button, #home > #cd-vertical-nav').removeClass('opacity-1').addClass('opacity-0');
            $(section).addClass('show');

            setTimeout(function () {
                $('#app .menu').css('z-index', '-1');
            }, 750)
        },

        showMenu: function (section) {
            $('#app .menu').removeClass('opacity-0').addClass('opacity-1').css('z-index', '');
            $('.equipments').removeClass('no-scroll').css('padding-right', '');
            $(section).removeClass('show');

            setTimeout(function () {
                $('.menu-button, .search-button, #home > #cd-vertical-nav').removeClass('opacity-0').addClass('opacity-1');
                $('.equipment--detail > div').animate({
                    scrollTop: 0
                }, 800);

                if ($(window).width() > 567) {
                    $('.vertical-menu li').removeClass('active');
                    $('.vertical-menu li').eq(0).addClass('active');
                }

                if ($(window).width() > 1200) {
                    $('.equipment--detail--content').removeClass('col-9');
                }

                if ($(window).width() > 992) {
                    $('.w-50').removeClass('w-75');
                }
            }, 500);
        },

        scroll: function (sections) {
            $.each(sections, function (index, value) {
                if ($('#' + value + ' > div').visible(true)) {
                    $('.vertical-menu li').removeClass('active');
                    $('.vertical-menu li').eq(index).addClass('active');

                    var img = '<img src="./frontend/images/ic_next.svg">';
                    var prev = $('.vertical-menu li.active').prev().find('a');
                    var next = $('.vertical-menu li.active').next().find('a');

                    if (prev.length > 0) {
                        $('.horizontal-menu .back').attr('href', prev.attr('href')).text(prev.text().split('. ')[1]).prepend(img);
                    } else {
                        $('.horizontal-menu .back').empty();
                    }

                    if (next.length > 0) {
                        $('.horizontal-menu .next').attr('href', next.attr('href')).text(next.text().split('. ')[1]).prepend(img);
                    } else {
                        $('.horizontal-menu .next').empty();
                    }
                    return '';
                }
            });
        },

        return: function () {
            var url = window.location.href;
            window.history.pushState(null, null, url.substring(0, url.indexOf('#')));

            if ($(window).width() <= 567) {
                $equipments.showMenu('.equipment--detail--mb');

                setTimeout(function () {
                    $('.equipment--detail--mb .equipment--detail--content section').removeAttr('style').removeClass('active');
                    $('.equipment--detail--mb .equipment--detail--content section').first().addClass('active');
                    $('.equipment--detail--mb .equipment--detail--menu .link.return').addClass('hidden');
                    $('.equipment--detail--mb .equipment--detail--menu .link.return').last().text('Tecnologia');
                }, 350);
            } else {
                $equipments.showMenu('.equipment--detail');
            }

            sections = [];
        }
    }

})();