var $mobileMain = null, $test = null;

(function () {
    'use strict';

    $mobileMain = {
        initialize: function () {
            // Set mobile classes
            $('html').removeClass('no-js').addClass('js csstransitions');
            $('#app .menu').addClass('down');
            $('.menu-logo svg #Group').attr('style', 'fill: #000;');

            // Set menu
            $mobileMain.setMenu();

            // Goto equipment detail
            $mobileMain.goTo();

            // Navigation clicks
            $('.link--page.menu--link').each(function () {
                if (!$(this).attr('data-menu')) {
                    $(this).click(function (ev) {
                        ev.preventDefault();

                        if ($(this).hasClass('active')) {
                            $mobileMain.closeMenu($('.menu-button'));
                        } else {
                            // change menu urls from #page to /page
                            var url = $(this).attr('href').slice(1);
                            url = url === 'home' ? '/' : '/' + url;

                            // Loading page
                            $('#app').addClass('loading-page');

                            // Set menu
                            $('.link--page').removeClass('active');
                            $(this).addClass('active');

                            // Close menu
                            $('#app .menu').addClass('down');
                            $mobileMain.closeMenu($('.menu-button'));

                            setTimeout(function () {
                                $mobileMain.loadPage(url);
                            }, 500);
                        }
                    });
                } else {
                    // Which menu to open or close?
                    if ($(this).attr('data-revert')) {
                        $(this).click(function (ev) {
                            ev.preventDefault();

                            $('.' + $(this).attr('data-revert')).removeAttr('style');
                            $('.' + $(this).attr('data-menu')).removeAttr('style');
                        });
                    } else {
                        $(this).click(function (ev) {
                            ev.preventDefault();

                            $('.menu--primary').attr('style', 'transform: translateX(-100%);');
                            $('.' + $(this).attr('data-menu')).attr('style', 'transform: translateX(0);');
                        });
                    }
                }
            });

            // Go to
            $('.btn--goTo').click(function () {
                // Loading page
                $('#app').addClass('loading-page');

                $mobileMain.loadPage('/' + $(this).attr('data-link'));
            });

            // Go to homepage
            $('.menu-logo').click(function () {
                window.location = '/';
            });

            // Open menu
            $('.menu-button').click(function () {
                if (!$(this).hasClass('menu-button--open')) {
                    $('#app .menu').removeClass('down');
                    $mobileMain.openMenu($(this));
                } else {
                    $('#app .menu').addClass('down');
                    $mobileMain.closeMenu($(this));
                }
            });

            // Open mobile search
            $('.search-button').click(function () {
                $(this).removeClass('search').addClass('_close');
                $('.search-nav--mb').removeClass('hidden');
            });

            // Close mobile search
            $('.search-button--close').click(function () {
                $(this).removeClass('_close').addClass('search');
                $('.search-nav--mb').addClass('hidden');
            });

            // Search links
            $(document).on('click', '.search-nav--mb .search-nav--result-list li a', function (ev) {
                console.log($(this).attr('href').indexOf('#'));
                if ($(this).attr('href').indexOf('#') >= 0) {
                    ev.preventDefault();

                    var target = $('.pages-stack .page .scroll-content').length > 0 ? $('.page:not(.page--inactive) .scrollbar-macosx > div') : $('.page:not(.page--inactive) > div');
                    target.animate({
                        scrollTop: 0
                    }, 0);

                    $('.search-button--close').click();

                    var elem = $(this);
                    setTimeout(function () {
                        var url = window.location.href;
                        window.history.pushState(null, null, url.substring(0, url.indexOf('/')) + elem.attr('href'));

                        $mobileMain.goTo();
                    }, 350);
                }
            });
        },

        openMenu: function (elem) {
            elem.addClass('menu-button--open');
            $('.menu-logo').hide();
            $('.search-button').hide();
            $('.pages-nav').addClass('pages-nav--open');
            $('.pages-stack').addClass('pages-stack--open');
            $('.page').css('transform', 'translate3d(0px, 120%, -200px)');
        },

        closeMenu: function (elem) {
            elem.removeClass('menu-button--open');
            $('.menu-logo').show();
            $('.search-button').show();
            $('.pages-nav').removeClass('pages-nav--open');
            $('.page').css('transform', 'translate3d(0px, 0px, 0px)');

            setTimeout(function () {
                $('.pages-stack').removeClass('pages-stack--open');
            }, 450);
        },

        setMenu: function () {
            if ($('.page').hasClass('services')) {
                // Show services menu
                $('.menu--primary').attr('style', 'transform: translateX(-100%);');
                $('.menu--services').attr('style', 'transform: translateX(0);');
            }

            $('.link--page').removeClass('active');
            $('.link--page[href="#' + $('.page').attr('id') + '"]').addClass('active');
        },

        goTo: function () {
            var target = $('.pages-stack .page .scroll-content').length > 0 ? $('.page:not(.page--inactive) .scrollbar-macosx > div') : $('.page:not(.page--inactive) > div'),
                url = window.location.href;

            if (url.indexOf('#') > -1) {
                var equipmentId = url.split('#')[1];

                target.animate({
                    scrollTop: $('.page:not(.page--inactive) #' + equipmentId).position().top
                }, 800);

                setTimeout(function () {
                    $('.page:not(.page--inactive) #' + equipmentId + ' .btn--container .btn').click();
                }, 800);
            }
        },

        loadPage: function (url) {
            $('#loading-screen').attr('style', 'animation: unset;');

            setTimeout(function () {
                $('#loading-screen').attr('style', 'animation: fadeOut 1s ease 3s both;');
            }, 50);

            $('#progress').addClass('animate');

            setTimeout(function () {
                $('#progress').removeClass('animate');
                $('#app').removeClass('loading-page');
                $('#loading-screen').removeAttr('style');

                $loadPages.initialize();
                $loadComponents.initialize();
            }, 4000);

            $.get(url, {fetchContent: true}, function (data) {
                // Replace page
                $('.pages-stack').replaceWith(data);
            });

            $.get(url, {type: 'seo'}, function (data) {
                $('title').text(data.title);
                $('meta[itemprop="name"], meta[itemprop="og:title"], meta[name="twitter:title"]').attr('content', data.title);
                $('meta[name="description"], meta[itemprop="description"], meta[name="og:description"], meta[name="twitter:description"]').attr('content', data.description);
                $('meta[name="keywords"]').attr('content', data.keywords);
                $('link[rel="canonical"]').attr('href', window.location.protocol + '//' + window.location.host + url);

                window.history.pushState(null, null, url);
            });
        }
    }

})();

$(document).ready(function () {
    if (window.innerWidth < 576) {
        $mobileMain.initialize();
    }
});