var $stackedCard = null;

(function () {
    'use strict';

    $stackedCard = {
        initialize: function () {
            $('.card-stack--container').each(function (index) {
                $(this).attr('id', 'card-stack--container-' + index);
            });

            $('.card-pagination li').click(function () {
                var parent = $(this).parents('.card-stack--container').attr('id');
                var elem = $(this);
                var index = elem.index();

                // Backwards
                if (index < $('#' + parent + ' .card-list li:first').attr('id')) {
                    $stackedCard.prev(parent, index, elem);
                } else {
                    $stackedCard.next(parent, index, elem);
                }
            });

            $('.quote .prev').click(function () {
                var parent = $(this).parents('.card-stack--container').attr('id');
                var elem = $('#' + parent + ' .card-pagination li.active').prev();
                if (elem.length == 0) {
                    elem = $('#' + parent + ' .card-pagination li:last');
                }
                var index = elem.index();

                $stackedCard.prev(parent, index, elem);
            });

            $('.quote .next').click(function () {
                var parent = $(this).parents('.card-stack--container').attr('id');
                var elem = $('#' + parent + ' .card-pagination li.active').next();
                var index = elem.index();

                $stackedCard.next(parent, index, elem);
            });
        },

        next: function (id, index, elem) {
            (function theLoop(i) {
                var delay = 350;

                if (index == i) {
                    delay = 0;
                }

                setTimeout(function () {
                    $('#' + id + ' .card-list li:first').addClass('next-card');

                    setTimeout(function () {
                        $('#' + id + ' .card-list li:first').detach().insertAfter('#' + id + ' .card-list li:last').removeClass('next-card');
                        $('#' + id + ' .card-pagination li').removeClass('active');
                        $('#' + id + ' .card-pagination li').eq($('#' + id + ' .card-list li:first').attr('id') - 1).addClass('active');
                    }, 300);

                    if ($('#' + id + ' .card-list li:first').attr('id') != index && index != -1) {
                        --i;
                        theLoop(i);
                    } else {
                        setTimeout(function () {
                            $('#' + id + ' .card-pagination li').removeClass('active');

                            if (index == -1) {
                                elem = $('#' + id + ' .card-pagination li:first');
                            }
                            elem.addClass('active');
                        }, 350);
                    }
                }, delay);
            })(index);
        },

        prev: function (id, index, elem) {
            (function theLoop(i) {
                var delay = 350;

                if (index == i) {
                    delay = 0;
                }

                setTimeout(function () {
                    $('#' + id + ' .card-list li:last').detach().insertBefore('#' + id + ' .card-list li:first').addClass('prev-card');

                    setTimeout(function () {
                        $('#' + id + ' .card-pagination li').removeClass('active');
                        $('#' + id + ' .card-pagination li').eq($('#' + id + ' .card-list li:first').attr('id') - 1).addClass('active');

                        if ($('#' + id + ' .card-list li:first').attr('id') != (index + 1)) {
                            --i;
                            theLoop(i);
                        } else {
                            setTimeout(function () {
                                $('#' + id + ' .card-list li').removeClass('prev-card');
                                $('#' + id + ' .card-pagination li').removeClass('active');
                                elem.addClass('active');
                            }, 350);
                        }
                    }, 0);
                }, delay);
            })(index);
        }
    }

})();