var $searchButton = null;

(function () {
    'use strict';

    $searchButton = {
        initialize: function () {
            if ($(window).width() > 576) {
                $('.search-button').click(function () {
                    if ($('.search-button').hasClass('active')) {
                        return false;
                    } else if ($('.search-button').hasClass('search')) {
                        $searchButton.open()
                    } else if ($('.search-button').hasClass('_close')) {
                        $searchButton.close()
                    }
                });
            }

            $('nav[class^="search-nav"] input').keydown(function () {
                var elem = $(this);

                setTimeout(function () {
                    if (elem.val() !== '') {
                        $.post('/search', {
                            search: elem.val(),
                            mobile: $(window).width() <= 567 ? true : false
                        }, function (data) {
                            $searchButton.search(data);
                        });
                    } else {
                        $searchButton.clear();
                    }
                }, 10);
            });
        },

        open: function () {
            $('nav[class^="search-nav"] input').focus();
            $('.search-button').addClass('active');

            $('#one').velocity({
                width: '0',
                height: '30px',
                borderRadius: '0',
                borderWidth: '1px',
                borderColor: '#000',
                rotateZ: '-45deg'
            }, {
                duration: 250,
                easing: 'easeOutSine',
                complete: function () {
                    $('#one').velocity({
                        top: '-1px',
                        left: '14px'
                    }, {
                        duration: 250,
                        easing: 'easeOutSine'
                    });
                }
            });

            $('#two').velocity({
                width: '0',
                height: '30px',
                top: '-39px',
                left: '14px',
                borderWidth: '1px',
                borderColor: '#000',
                rotateZ: '225deg'
            }, {
                duration: 500,
                easing: 'easeOutSine',
                complete: function () {
                    $('.wrapper').removeClass('active');
                }
            });

            $('.search-button').addClass('_close').removeClass('search');
            $('.menu-logo').attr('style', 'visibility: hidden;');
        },

        close: function () {
            $('.search-button').addClass('active');

            $('#one').velocity('reverse', {
                duration: 250,
                easing: 'easeOutSine',
                complete: function () {
                    $('#one').velocity({
                        width: '20px',
                        height: '20px',
                        borderWidth: '2px',
                        borderColor: '#000',
                        borderRadius: '50%'
                    }, {
                        duration: 250,
                        easing: 'easeOutSine'
                    });
                }
            });

            $('#two').velocity({
                width: '5px',
                height: '15px',
                top: '-14px',
                left: '20px',
                borderWidth: '2px',
                borderColor: '#000',
                rotateZ: '-45deg'
            }, {
                duration: 500,
                easing: 'easeOutSine',
                complete: function () {
                    $('.search-button').removeClass('active');
                }
            });

            // Reset
            $('.search-button').addClass('search').removeClass('_close');
            $('.menu-logo').removeAttr('style');
            $searchButton.clear();
        },

        closeMB: function () {
            // toggle the search mb
            $('.search-nav--mb').addClass('hidden');
            // clear input search
            $('.search-nav--mb').find('input').val('');
            // Reset
            $searchButton.clear();
        },

        clear: function () {
            $('.search-nav--result-list').empty();
            $('.search--results--counter').empty();

            if ($(window).width() > 576) {
                $('.search-nav .search input').removeClass('searching');
                $('.search-nav .search-tips-container').removeClass('d-none opacity-0');
                $('.search-nav .search-nav--result-list--container').removeClass('opacity-1').addClass('d-none');
            }
        },

        search: function (data) {
            if ($(window).width() > 576) {
                $('.scrollbar-macosx').scrollbar();
                $('.search-nav .search-nav--result-list').html(data);
                $('.search-nav .search--results--counter').html($('.search-nav .search-nav--result-list li').length);

                if (!$('.search-nav .search input').hasClass('searching')) {
                    $('.search-nav .search input').addClass('searching');
                    $('.search-nav .search-tips-container').addClass('d-none opacity-0');
                    $('.search-nav .search-nav--result-list--container').addClass('opacity-1').removeClass('d-none');
                }
            } else {
                $('.search-nav--mb .search-nav--result-list').html(data);
                $('.search-nav--mb .search--results--counter').html($('.search-nav--mb .search-nav--result-list li').length);
            }
        }
    }

})();