var fragranceWizardForm = {
    name: '',
    email: '',
    message: '',
    space: '',
    business: '',
    dimensions: '',
    values: []
}, $fragranceWizard = null;

// Fragrance Wizard
(function () {
    'use strict';

    $fragranceWizard = {
        validation: function (step) {
            switch (step) {
                case 0:
                    return true;
                case 1:
                    return true;
                case 2:
                    return true;
                case 3:
                    if (fragranceWizardForm.attributes.length === 0) {
                        toastr.warning('Escolha uma ou mais de entre as opções em baixo.', 'Atenção!');
                        return false;
                    }
                    return true;
                default:
                    return true;
            }
        },

        getWizardInfo: function (elem) {
            switch ($('.show-wizard .wizard-steps li.current').index()) {
                case 0:
                    fragranceWizardForm.space = elem.find('.wizard-card--title').text();
                    break;
                case 1:
                    fragranceWizardForm.business = elem.find('.wizard-card--title').text();
                    break;
                case 2:
                    fragranceWizardForm.dimensions = $('.wizard-range-slider--value').text();
                    break;
                case 3:
                    var attributes = [];
                    $('.show-wizard .feature-list--item.selected').each(function () {
                        attributes.push($(this).find('.feature-list--item--text').text());
                    });
                    fragranceWizardForm.attributes = attributes;
                    break;
                case 4:
                    fragranceWizardForm.name = $('.show-wizard #name').val();
                    fragranceWizardForm.email = $('.show-wizard #email').val();
                    fragranceWizardForm.phone = $('.show-wizard #phone').val();
                    fragranceWizardForm.message = $('.show-wizard #message').val();
                    break;
                default:
                // code block
            }
        },

        setBusiness: function (elem) {
            $('.show-wizard .wizard-pane:nth-child(3)').find('img').attr('src', elem.find('.img--hover').attr('src'));
            $('.show-wizard .wizard-pane:nth-child(3)').find('.wizard-step--title').text('Qual o Tamanho do ' + elem.find('.wizard-card--title').text() + '?');
            $('.show-wizard .wizard-pane:nth-child(3)').find('.wizard-card--title').text(elem.find('.wizard-card--title').text());
        },

        reset: function () {
            // Step 1 & Step 2
            $('.wizard--fragrances .wizard-card.selected').find('.wizard-card--image-container img').eq(0).removeClass('hidden');
            $('.wizard--fragrances .wizard-card.selected').find('.wizard-card--image-container img').eq(1).addClass('hidden');
            $('.wizard--fragrances .wizard-card').removeClass('selected');
            // Step 3
            $('.wizard--fragrances .wizard-pane:nth-child(3)').find('img').attr('src', '');
            $('.wizard--fragrances .wizard-pane:nth-child(3)').find('.wizard-step--title').text('');
            $('.wizard--fragrances .wizard-pane:nth-child(3)').find('.wizard-card--title').text('');
            $('.wizard--fragrances .wizard-range-slider--value').text('menos de 45m2');
            $('.wizard--fragrances .asRange').asRange('val', '0');
            // Step 4
            $('.wizard--fragrances .feature-list--item.selected').removeClass('selected');

            fragranceWizardForm = {
                name: '',
                email: '',
                message: '',
                space: '',
                business: '',
                dimensions: '',
                values: []
            };
        }
    }

})();