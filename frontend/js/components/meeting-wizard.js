var meetingWizardForm = {
    name: '',
    email: '',
    message: '',
    date: '',
    hour: ''
}, $meetingWizard = null, options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};

(function () {
    'use strict';

    $meetingWizard = {
        initialize: function () {
            $('#calendar').fullCalendar({
                header: {
                    left: 'month',
                    center: 'prev,title,next',
                    right: 'today'
                },
                contentHeight: 450,
                dayClick: function (date, jsEvent, view) {
                    meetingWizardForm.date = date.format();

                    // Reset
                    $('.fc-day-top').removeAttr('style');
                    $('.fc-day-top .fc-day-number').removeAttr('style');
                    $('.fc-day').removeAttr('style');

                    // Date selected
                    $('td[data-date="' + meetingWizardForm.date + '"]').css('background-image', 'linear-gradient(45deg,#E1CB5E 0,#D6BC4F 37%,#D4B84C 60%,#BF9B30 100%)');
                    $('td[data-date="' + meetingWizardForm.date + '"] .fc-day-number').css('color', '#000');
                    $(this).css('background-image', 'linear-gradient(45deg,#E1CB5E 0,#D6BC4F 37%,#D4B84C 60%,#BF9B30 100%)');

                    var date = new Date(meetingWizardForm.date);
                    $('.demonstration-date').text(date.getDay() + ' de ' + $meetingWizard.getMonth(date.getMonth()) + ' ' + date.getFullYear());
                }
            });

            $('#calendar2').fullCalendar({
                header: {
                    left: '',
                    center: 'title',
                    right: ''
                },
                contentHeight: 450
            });

            $('#timepicker').mdtimepicker();
            $('#timepicker').mdtimepicker().on('timechanged', function (e) {
                $('.demonstration-date').text($('.demonstration-date').text().split(' às')[0] + ' às ' + e.value);
            });
        },

        validation: function (step) {
            switch (step) {
                case 0:
                    if (meetingWizardForm.date === '') {
                        toastr.warning('Escolha o seu dia preferido para reunir.', 'Atenção!');
                        return false;
                    }
                    return true;
                case 1:
                    if ($('#timepicker').val() === '') {
                        toastr.warning('Escolha a baixo o horário pretendido.', 'Atenção!');
                        return false;
                    }
                    return true;
                default:
                    return true;
            }
        },

        getMonth: function (month) {
            var $month = null;

            switch (month) {
                case 2:
                    $month = 'Fevereiro';
                    break;
                case 3:
                    $month = 'Março';
                    break;
                case 4:
                    $month = 'Abril';
                    break;
                case 5:
                    $month = 'Junho';
                    break;
                case 6:
                    $month = 'Julho';
                    break;
                case 7:
                    $month = 'Agosto';
                    break;
                case 8:
                    $month = 'Setembro';
                    break;
                case 9:
                    $month = 'Outubro';
                    break;
                case 10:
                    $month = 'Novembro';
                    break;
                case 11:
                    $month = 'Dezembro';
                    break;
                default:
                    $month = 'Janeiro';
                    break;
            }

            return $month;
        },

        getWizardInfo: function () {
            return meetingWizardForm = {
                name: $('.show-wizard #name').val(),
                email: $('.show-wizard #email').val(),
                phone: $('.show-wizard #phone').val(),
                message: $('.show-wizard #message').val(),
                date: $('#calendar').fullCalendar('getDate').format('MMM Do YY'),
                hour: $('#timepicker').val()
            };
        },

        reset: function () {
            // Step 1 & 2
            $('#calendar, #calendar2').fullCalendar('destroy');
            $('#timepicker').removeAttr('data-time').val('');

            meetingWizardForm = {
                name: '',
                email: '',
                message: '',
                date: '',
                hour: ''
            };
        }
    }

})();