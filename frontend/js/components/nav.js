var $nav = null;

(function () {
    'use strict';

    $nav = {
        initialize: function () {
            var target = $('.pages-stack .page .scroll-content').length > 0 ? $('.pages-stack .page .scroll-content') : $('.pages-stack .page > div');

            if ($(window).width() > 576) {
                target.scroll(function () {
                    $nav.setNavbar($(this));
                });
            }
        },

        setNavbar: function (elem) {
            if ($('.show-wizard').length === 0) {
                var st = elem.scrollTop();

                if (st !== 0) {
                    // downscroll code
                    $('#app .menu').addClass('down');
                    $('.menu-logo svg #Group').attr('style', 'fill: #000;');
                } else {
                    // upscroll code
                    if (st === 0) {
                        $('#app .menu').removeClass('down');

                        if ($('#app').hasClass('white')) {
                            $('.menu-logo svg #Group').attr('style', 'fill: #fff;');
                        }
                    }
                }
            }
        },

        reset: function () {
            $('.search-button .element').removeAttr('style');
            $nav.setNavbar($('.page:not(.page--inactive)'));
        }
    }

})();