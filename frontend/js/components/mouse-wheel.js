var $mouseWheel = null;

(function () {
    'use strict';

    $mouseWheel = {
        initialize: function () {
            function lineEq(y2, y1, x2, x1, currentVal) {
                // y = mx + b
                var m = (y2 - y1) / (x2 - x1), b = y1 - m * x1;
                return m * currentVal + b;
            };

            var distanceThreshold = {min: 0, max: 100};

            /**************** Scroll Icon ****************/
            var iconWrapperScroll = document.querySelector('.scroll');

            if (iconWrapperScroll) {
                var iconScrollButton = iconWrapperScroll.parentNode;
                var scrollWheel = iconWrapperScroll.querySelector('.scroll__wheel');
                var scrollInterval = {from: 1, to: 15};

                var tweenScroll = TweenMax.to(scrollWheel, 5, {
                    repeat: -1,
                    yoyo: false,
                    y: 32,
                    scaleY: 0,
                    paused: true
                });

                var stateScroll = 'paused';
                new Nearby(iconScrollButton, {
                    onProgress: function (distance) {
                        var time = lineEq(scrollInterval.from, scrollInterval.to, distanceThreshold.max, distanceThreshold.min, distance);
                        tweenScroll.timeScale(Math.min(Math.max(time, scrollInterval.from), scrollInterval.to));

                        if (distance < distanceThreshold.max && distance >= distanceThreshold.min && stateScroll !== 'running') {
                            tweenScroll.play();
                            stateScroll = 'running';
                        } else if ((distance > distanceThreshold.max || distance < distanceThreshold.min) && stateScroll !== 'paused') {
                            tweenScroll.pause();
                            stateScroll = 'paused';
                            tweenScroll.time(0);
                        }
                    }
                });
            }
        }
    }

})();