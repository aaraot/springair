var $loadComponents = null;

(function () {
    'use strict';

    $loadComponents = {
        initialize: function () {
            $input.initialize();
            $mouseWheel.initialize();
            $nav.initialize();
            $searchButton.initialize();
            $stackedCard.initialize();
            $svg.initialize();
        }
    }

})();

$(document).ready(function () {
    $loadComponents.initialize();
});