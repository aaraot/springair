var $svg = null;

(function () {
    'use strict';

    $svg = {
        initialize: function () {
            $(document).on('click', '.box', function () {
                if (!$(this).find('.inner-box').hasClass('fill')) {
                    $(this).find('.inner-box').addClass('fill');
                    $(this).parent().css('overflow', 'hidden');
                    $(this).nextAll().addClass('animate-svg');
                } else {
                    $(this).nextAll().addClass('reverse');

                    var elem = $(this);
                    setTimeout(function () {
                        elem.find('.inner-box').removeClass('fill');
                        elem.parent().css('overflow', '');
                        elem.nextAll().removeClass('reverse animate-svg');
                    }, 900);
                }
            });
        }
    }

})();