var $input = null;

(function () {
    'use strict';

    $input = {
        initialize: function () {
            if ($('.mat-input').length > 0) {
                $('.mat-input').on('focus', function () {
                    $(this).parent().addClass('is-active is-completed');
                });

                $('.mat-input').on('focusout', function () {
                    if ($(this).val() === '') {
                        $(this).parent().removeClass('is-completed');
                    }
                    $(this).parent().removeClass('is-active');
                });
            }
        }
    }

})();