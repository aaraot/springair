var $customWizard = null, wizard = null, wizardForm = null, confetti = null;

(function () {
    'use strict';

    $customWizard = {
        initialize: function () {
            wizard = $('.show-wizard .wizard').wizard();

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            var img = '', wizardCardImageSrc = '';
            $('.wizard-card--image-container').hover(function () {
                img = $(this).find('.img');

                wizardCardImageSrc = img.attr('src');
                img.attr('src', img.next().attr('src'));
            }, function () {
                img.attr('src', wizardCardImageSrc);
            });

            $('.show-wizard .wizard-steps li').click(function () {
                if ($(this).attr('data-completed')) {
                    wizard.wizard('goTo', $(this).index());
                }
            });

            $('.show-wizard .wizard-card, .show-wizard .btn-next-step').click(function () {
                if ($('.wizard-pane.active').find('.final-step')) {
                    setTimeout(function () {
                        $('.wizard-pane.active').find('.final-step').eq(0).removeClass('hidden');
                    }, 350);
                }

                var isValid = true;
                if ($('.show-wizard').hasClass('wizard--services')) {
                    isValid = $meetingWizard.validation($customWizard.currentIndex());

                    if (isValid) {
                        // step 2 preparation
                        $('#calendar2').fullCalendar('gotoDate', meetingWizardForm.date);
                        $('td[data-date="' + meetingWizardForm.date + '"]').css('background-image', 'linear-gradient(45deg,#E1CB5E 0,#D6BC4F 37%,#D4B84C 60%,#BF9B30 100%)');
                        $('td[data-date="' + meetingWizardForm.date + '"] .fc-day-number').css('color', '#000');

                        var week = null;
                        $('#calendar2 .fc-week .fc-day-top').each(function () {
                            if ($(this).attr('data-date') === meetingWizardForm.date) {
                                week = $(this).parents('.fc-week').index();
                            }
                        });

                        $('#calendar2 .fc-week:not(:eq("' + week + '"))').remove();
                        $('#calendar2 .fc-body .fc-scroller').height($('#calendar2 .fc-body .fc-scroller .fc-day-grid').height());
                        $('#calendar2 .fc-view-container').height($('#calendar2 .fc-view').height());
                    }
                } else if ($('.show-wizard').hasClass('wizard--fragrances')) {
                    $fragranceWizard.getWizardInfo($(this));
                    isValid = $fragranceWizard.validation($customWizard.currentIndex());
                }

                if ($(this).hasClass('wizard-card') && !$(this).attr('data-next')) {
                    $(this).siblings().removeClass('selected');
                    $(this).siblings().find('.wizard-card--image-container img').eq(0).removeClass('hidden');
                    $(this).siblings().find('.wizard-card--image-container img').eq(1).addClass('hidden');
                    $(this).addClass('selected');
                    $(this).find('.wizard-card--image-container img').eq(0).addClass('hidden');
                    $(this).find('.wizard-card--image-container img').eq(1).removeClass('hidden');
                }

                if ($(this).attr('data-goTo')) {
                    $('.show-wizard .wizard-steps li').eq(parseInt($(this).attr('data-goTo')) - 1).attr('style', 'cursor: not-allowed;');
                    wizard.wizard('goTo', parseInt($(this).attr('data-goTo')));

                    for (var i = 0; i < $(this).attr('data-goTo'); i++) {
                        $('.show-wizard .wizard-steps li').eq(i).removeClass('disabled').addClass('done');

                        $customWizard.completed($('.show-wizard .wizard-steps li').eq(i));
                    }
                }

                if (!$(this).attr('data-next')) {
                    if ($(this).hasClass('wizard-card')) {
                        if ($('.show-wizard').hasClass('wizard--fragrances')) {
                            $fragranceWizard.setBusiness($(this));
                        }
                    }

                    if (isValid) {
                        wizard.wizard('next');
                    }

                    setTimeout(function () {
                        $customWizard.completed($('.show-wizard .done').last());
                    }, 350);
                }

                setTimeout(function () {
                    if ($('.show-wizard .wizard-pane.active').find('.wizard-card').length > 0) {
                        if ($('.show-wizard .wizard-pane.active').find('.selected').length == 0) {
                            if ($('.show-wizard .wizard-steps .current').find('.checkmark').is(':visible')) {
                                $('.show-wizard .wizard-steps .current').removeAttr('style');
                                $('.show-wizard .wizard-steps .current').find('.checkmark').toggle();
                                $('.show-wizard .wizard-steps .current').find('.checkmark').prev().attr('style', 'visibility: visible;');
                            }
                        }
                    }
                }, 350);
            });

            wizardForm = $('.show-wizard form').validate($formRules);

            $('.show-wizard form').on('submit', function (e) {
                e.preventDefault();

                if (wizardForm.valid()) {
                    if ($('.show-wizard').hasClass('wizard--services')) {
                        $.post('/meeting', {data: $meetingWizard.getWizardInfo()}, function (data) {
                            $customWizard.submitted(data);
                        });
                    }

                    if ($('.show-wizard').hasClass('wizard--fragrances')) {
                        $fragranceWizard.getWizardInfo(null);
                        $.post('/wizard-fragrances', {data: fragranceWizardForm}, function (data) {
                            $customWizard.submitted(data);
                        });
                    }
                }
            });
        },

        currentIndex: function () {
            return $('.show-wizard li.current').index();
        },

        submitted: function (response) {
            if (response.submitted) {
                $('.show-wizard .wizard-steps').addClass('animated fadeOutUp');
                $('.show-wizard .final-step').eq(0).addClass('animated fadeOutUp');

                setTimeout(function () {
                    $('.show-wizard .wizard-steps').hide();
                    $('.show-wizard .final-step').eq(0).addClass('hidden');
                    $('.show-wizard .wizard-pane').css('overflow-y', 'hidden');
                }, 500);

                setTimeout(function () {
                    confetti = new window.ConfettiGenerator({
                        "target": $('.show-wizard canvas[id^="confetti"]').attr('id'),
                        "max": "20",
                        "size": "2",
                        "animate": true,
                        "props": ["square"],
                        "colors": [[191, 155, 48], [241, 235, 220], [235, 226, 201], [238, 231, 210]],
                        "clock": "4"
                    });
                    confetti.render();
                    $('.show-wizard .final-step').eq(1).removeClass('hidden').addClass('animated fadeInUp');
                }, 500);

                $customWizard.completed($('.show-wizard .current'));
            } else {
                toastr.error('Ocorreu um erro. Por favor, tente submeter o formulário outra vez.', 'Erro!');
            }
        },

        completed: function (elem) {
            if (!elem.find('.checkmark').is(':visible')) {
                if (fragranceWizardForm.space != 'Casa') {
                    elem.attr('data-completed', true);
                }

                elem.find('.checkmark').toggle();
                elem.find('.checkmark').prev().attr('style', 'visibility: hidden;');
            }
        },

        reset: function () {
            $('.wizard .wizard-steps li').each(function () {
                if ($(this).find('.checkmark').prev().attr('style')) {
                    $(this).find('.checkmark').toggle();
                    $(this).find('.checkmark').prev().removeAttr('style');
                    $(this).removeClass('done').removeAttr('data-completed');
                }
            });

            // Final Step
            $('.wizard .wizard-steps').show().removeClass('animated fadeOutUp');
            $('.mat-div.is-completed').removeClass('is-completed');
            $('.mat-input').val('');
            $('.wizard .wizard-pane').removeAttr('style');
            $('.final-step').removeClass('hidden animated fadeOutUp');
            $('.final-step.text-center').addClass('hidden').removeClass('animated fadeInUp');
            if (confetti !== null) {
                confetti.clear();
            }
        }
    }

})();