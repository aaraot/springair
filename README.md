# Springair Website

Springair website redesign.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Xampp [https://www.apachefriends.org/index.html]

Composer [https://getcomposer.org/]

Node.js [https://nodejs.org/en/]

Note: If you are using an IDE like PhpStorm or WebStorm, you must install Ruby and Ruby Sass.
      This way you can use the file watcher to compile your scss files into css files. 

Ruby [https://rubyinstaller.org/]

Ruby Sass [https://sass-lang.com/ruby-sass]

Note: I like to configure my own domains and for this i use this software to easily edit
      and manage the hosts file for windows.
      
Hosts File Editor [https://hostsfileeditor.com/]
```

### Installing

A step by step series of examples that tell you how to get a development env running

#### First step

```
Install the DB into PhpMyAdmin from /springair/db/springair.sql
```

#### Second step

Open the console and type:

```
npm run watch
```

## Deployment

### First Step

Enter into Springair cPanel with the provided credentials, make a backup of the DB and files and name them 'backup_dd_mm_yy'.

### Second Step

Export the DB from your PhpMyAdmin and paste it into /springair/db folder and replace the old one inside it.

### Third Step

Create a .zip of all the files in the project folder and remove the uploads folder and /springair/local/.env file from the .zip file.

### Fourth Step

Upload the .zip file into Springair cPanel.

### Fifth Step

Extract the .zip file and install the new DB (if changed) into the cPanel PhpMyAdmin. 

### Sixth Step

Go to /root/local/resources/views/layouts/springair/app.blade.php and uncomment the line 241 and comment the line 242.

Done!

## Built With

### Frameworks
* [Laravel](https://laravel.com/)
* [Bootstrap](https://getbootstrap.com/)

### Package Manager
* [Npm](https://www.npmjs.com/)

### Libraries
* [jQuery](https://jquery.com/)
* [Font Awesome](https://fontawesome.com/)

### Stylesheet Language
* [Sass](https://sass-lang.com/)

### CRUD Builder
* [Sximo](http://sximoinc.com/)

### Plugins
* [Animatecss](https://daneden.github.io/animate.css/)
* [Confetti-js](https://www.npmjs.com/package/confetti-js)
* [Full Calendar](https://fullcalendar.io/)
* [jQuery-asRange](https://github.com/amazingSurge/jquery-asRange)
* [jQuery Validate](https://jqueryvalidation.org/)
* [jQuery Wizard](https://github.com/amazingSurge/jquery-wizard)
* [jQuery Scrollbar](https://github.com/gromo/jquery.scrollbar/)
* [Material Time Picker](https://github.com/dmuy/MDTimePicker)
* [Page Stack Navigation](http://www.codrops.com)
* [Proximity Feedback](http://www.codrops.com)
* [Select2](https://select2.org/)
* [Slick](http://kenwheeler.github.io/slick/)
* [Toastr](https://github.com/CodeSeven/toastr)

### Mobile Plugins
* [jQuery TouchSwipe](http://plugins.jquery.com/project/touchSwipe) 

## Authors

* **Aarão Teixeira** - *Full Stack Developer* - [Aarão Teixeira](https://gitlab.com/aaraot)
* **Bruno Charters** - *UX/UI Design* - [Bruno Charters](https://dribbble.com/BrunoCharters)
* **Catarina Carvalho** - *Graphic Designer* - [Oak-ay](https://dribbble.com/catvcarvalho)
* **Nicolas Matias** - *Loading* - [Nicolas Matias](https://dribbble.com/Nicolasmatias)

## Acknowledgments

* Dane Den
* Amazing Surge
* Gromo
* Dmuy
* Kenwheeler
* CodeSeven
* Codrops (Inspiration)